/*BEGIN_LEGAL
Intel Open Source License

Copyright (c) 2002-2018 Intel Corporation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.  Redistributions
in binary form must reproduce the above copyright notice, this list of
conditions and the following disclaimer in the documentation and/or
other materials provided with the distribution.  Neither the name of
the Intel Corporation nor the names of its contributors may be used to
endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE INTEL OR
ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
END_LEGAL */

#include "fase.hpp"

#include <iomanip>

uint64_t num_WU_insts = 0;
uint64_t num_vml_insts = 0;
uint64_t num_fp_insts = 0;
uint64_t num_fma_insts = 0;
uint64_t num_total_insts = 0;
uint64_t num_instrumented_regs = 0;
uint64_t num_src_reg_calls = 0;
uint64_t num_dest_reg_calls = 0;

#ifdef STATS_PROCESSED
uint64_t num_fma_processed_insts = 0;
#endif

#ifdef AVX512
/**
 * This function is used to analyze instructions where one of the operands of the FMA instruction
 * comes from memory. Additionally this operand is broadcasted as the operand2 in the complete
 * AVX512 instruction. This means is repeated in a complete 16 elements __mm512 variable.
 * The number of bytes loaded are equal to 4.
**/
void InstrumentationOperandsBcastDecorator(const OPCODE operation, PIN_REGISTER *operand1,
                                           ADDRINT* operand2, PIN_REGISTER *operand3,
                                           PIN_REGISTER *destination)
{
#ifdef STATS_ON
    num_src_reg_calls++;
#endif

#ifdef STATS_PROCESSED
    num_fma_processed_insts++;
#endif

#ifdef LOGGING
    *out << OPCODE_StringShort(operation) << "_bcast_op2 " << endl; //" numbytes: " << numbytes << endl;
#endif

#ifndef STATS_PROCESSED
    process_operands_bcast_operand2((__m512*)&(operand1->flt[0]),
                     *(float*)operand2,
                     (__m512*)&(operand3->flt[0]),
                     (__m512*)&(destination->flt[0]));
#endif

}
/**
 * This function is used to analyze instructions where one of the operands of the FMA instruction
 * comes from memory. Additionally this operand is broadcasted as the operand3 in the complete
 * AVX512 instruction. This means is repeated in a complete 16 elements __mm512 variable.
 * The number of bytes loaded are equal to 4.
**/
void InstrumentationOperandsBcastDecorator2(const OPCODE operation, PIN_REGISTER *operand1,
                                           PIN_REGISTER *operand2, ADDRINT* operand3,
                                           PIN_REGISTER *destination)
{
#ifdef STATS_ON
    num_src_reg_calls++;
#endif

#ifdef STATS_PROCESSED
    num_fma_processed_insts++;
#endif

#ifdef LOGGING
    *out << OPCODE_StringShort(operation) << "_bcast_op3 " << endl;
#endif

#ifndef STATS_PROCESSED
    process_operands_bcast_operand3((__m512*)&(operand1->flt[0]),
                     (__m512*)&(operand2->flt[0]),
                     *(float*)operand3,
                     (__m512*)&(destination->flt[0]));
#endif
}
/**
 * This function is used to analyze instructions where one of the operands of the FMA instruction
 * comes from memory (Operand2). Additionally this operand is completely loaded from memory, 64 bytes.
**/
void InstrumentationOperandsMem64Bytes(const OPCODE operation, PIN_REGISTER *operand1,
                                       ADDRINT* operand2, PIN_REGISTER *operand3,
                                       PIN_REGISTER *destination, PIN_REGISTER *mask,
                                       BOOL is_masking, BOOL is_zeroing, BOOL is_merging)
{
#ifdef STATS_ON
    num_src_reg_calls++;
#endif

#ifdef STATS_PROCESSED
    num_fma_processed_insts++;
#endif

#ifdef LOGGING
    *out << OPCODE_StringShort(operation) << "_mem64_op2 " << endl;
#endif

#ifndef STATS_PROCESSED
    __m512 tmp_operand_2;
    PIN_SafeCopy((void *)&tmp_operand_2, (const void *) operand2, 64);
    process_operands((__m512*)&(operand1->flt[0]),
                     (__m512*)&tmp_operand_2,
                     (__m512*)&(operand3->flt[0]),
                     (__m512*)&(destination->flt[0]),
                     (__mmask16*)&(mask->word[0]),
                     is_masking, is_zeroing, is_merging);
#endif

}
/**
 * This function is used to analyze instructions where one of the operands of the FMA instruction
 * comes from memory (Operand3). Additionally this operand is completely loaded from memory, 64 bytes.
**/
void InstrumentationOperandsMem64Bytes2(const OPCODE operation, PIN_REGISTER *operand1,
                                        PIN_REGISTER *operand2, ADDRINT* operand3,
                                        PIN_REGISTER *destination, PIN_REGISTER *mask,
                                        BOOL is_masking, BOOL is_zeroing, BOOL is_merging)
{
#ifdef STATS_ON
    num_src_reg_calls++;
#endif

#ifdef STATS_PROCESSED
    num_fma_processed_insts++;
#endif

#ifdef LOGGING
    *out << OPCODE_StringShort(operation) << "_mem64_operand3 " << endl;
#endif

#ifndef STATS_PROCESSED
    __m512 tmp_operand_3;
    PIN_SafeCopy((void *)&tmp_operand_3, (const void *) operand3, 64);
    process_operands((__m512*)&(operand1->flt[0]),
                     (__m512*)&(operand2->flt[0]),
                     (__m512*)&tmp_operand_3,
                     (__m512*)&(destination->flt[0]),
                     (__mmask16*)&(mask->word[0]),
                     is_masking, is_zeroing, is_merging);
#endif
}

/**
 * This function is used to analyze instructions when all operands comes from registers.
**/
void InstrumentationOperandsRegs(const OPCODE operation, PIN_REGISTER *operand1,
                                PIN_REGISTER *operand2, PIN_REGISTER *operand3,
                                PIN_REGISTER *destination, PIN_REGISTER *mask,
                                BOOL is_masking, BOOL is_zeroing, BOOL is_merging)
{
#ifdef STATS_ON
    num_src_reg_calls++;
#endif
#ifdef LOGGING
    *out << OPCODE_StringShort(operation) << "_regs " << endl;
#endif

#ifdef STATS_PROCESSED
    num_fma_processed_insts++;
#endif

#ifndef STATS_PROCESSED
    process_operands((__m512*)&(operand1->flt[0]),
                     (__m512*)&(operand2->flt[0]),
                     (__m512*)&(operand3->flt[0]),
                     (__m512*)&(destination->flt[0]),
                     (__mmask16*)&(mask->word[0]),
                     is_masking, is_zeroing, is_merging);
#endif
}

void PrintRealDestinationContent(PIN_REGISTER *destination)
{
    *out << "Real Result:" << endl;
    for (int i = 0; i < 16; i++)
    {
        *out << "0x" << FLT32_TO_HEX(destination->flt[i]) << " ";
    }
    *out << endl;
}
#endif

/**
 * This function is used to analyze instructions when all operands comes from registers.
 * Gives support to 32 bytes instructions. (256bits)
**/
void InstrumentationOperandsRegs256(const OPCODE operation, PIN_REGISTER *operand1,
                                PIN_REGISTER *operand2, PIN_REGISTER *operand3,
                                PIN_REGISTER *destination)
{
#ifdef STATS_ON
    num_src_reg_calls++;
#endif
#ifdef LOGGING
    *out << OPCODE_StringShort(operation) << "_regs_256 " << endl;
#endif

#ifdef STATS_PROCESSED
    num_fma_processed_insts++;
#endif

#ifndef STATS_PROCESSED
    process_operands_256((__m256*)&(operand1->flt[0]),
                     (__m256*)&(operand2->flt[0]),
                     (__m256*)&(operand3->flt[0]),
                     (__m256*)&(destination->flt[0]));
#endif
}
/**
 * This function is used to analyze instructions where one of the operands of the FMA instruction
 * comes from memory (Operand2). Additionally this operand is completely loaded from memory, 32 bytes.
 * Gives support to 256 bits instructions
**/
void InstrumentationOperandsMem32Bytes(const OPCODE operation, PIN_REGISTER *operand1,
                                           ADDRINT* operand2, PIN_REGISTER *operand3,
                                           PIN_REGISTER *destination)
{
#ifdef STATS_ON
    num_src_reg_calls++;
#endif

#ifdef STATS_PROCESSED
    num_fma_processed_insts++;
#endif

#ifdef LOGGING
    *out << OPCODE_StringShort(operation) << "_mem32_op2 " << endl;
#endif

#ifndef STATS_PROCESSED
    __m256 tmp_operand_2;
    PIN_SafeCopy((void *)&tmp_operand_2, (const void *) operand2, 32);
    process_operands_256((__m256*)&(operand1->flt[0]),
                         (__m256*)&tmp_operand_2,
                         (__m256*)&(operand3->flt[0]),
                         (__m256*)&(destination->flt[0]));
#endif

}

/**
 * This function is used to analyze instructions where one of the operands of the FMA instruction
 * comes from memory (Operand3). Additionally this operand is completely loaded from memory, 32 bytes.
 * Gives support to 256 bits instructions
**/void InstrumentationOperandsMem32Bytes2(const OPCODE operation, PIN_REGISTER *operand1,
                                        PIN_REGISTER *operand2, ADDRINT* operand3,
                                        PIN_REGISTER *destination)
{
#ifdef STATS_ON
    num_src_reg_calls++;
#endif

#ifdef STATS_PROCESSED
    num_fma_processed_insts++;
#endif

#ifdef LOGGING
    *out << OPCODE_StringShort(operation) << "_mem32_operand3 " << endl;
#endif

#ifndef STATS_PROCESSED
    __m256 tmp_operand_3;
    PIN_SafeCopy((void *)&tmp_operand_3, (const void *) operand3, 32);
    process_operands_256((__m256*)&(operand1->flt[0]),
                         (__m256*)&(operand2->flt[0]),
                         (__m256*)&tmp_operand_3,
                         (__m256*)&(destination->flt[0]));
#endif
}

void PrintRealDestinationContent256(PIN_REGISTER *destination)
{
    *out << "Real Result 256:" << endl;
    for (int i = 0; i < 8; i++)
    {
        *out << "0x" << FLT32_TO_HEX(destination->flt[i]) << " ";
    }
    *out << endl;
}

VOID CountAllInstructions(UINT32 num)
{
    num_total_insts += num;
}

VOID CountFPInstructions(UINT32 num)
{
    num_fp_insts += num;
}

VOID CountFMAInstructions(UINT32 num)
{
    num_fma_insts += num;
}

#ifdef STATS_PROCESSED
VOID CountProcessedFMAInstructions(UINT32 num)
{
    num_fma_processed_insts += num;
}
#endif

VOID CountVMLInstructions(UINT32 num)
{
    num_vml_insts += num;
}

VOID CountWUInstructions(UINT32 num)
{
    num_WU_insts += num;
}

VOID Trace_Native(TRACE trace, VOID *v){
    PIN_Detach();
}

// Pin calls this function every time a new trace (single entry, multiple exits)
// is executed, after the first detach
VOID Trace(TRACE trace, VOID *v)
{
    string routineName;
#ifndef ROUTINES_ON
    UINT32 numOperands;
    UINT32 size;
#endif
    for (BBL bbl = TRACE_BblHead(trace); BBL_Valid(bbl); bbl = BBL_Next(bbl))
    {
#ifdef STATS_ON
        BBL_InsertCall(bbl, IPOINT_ANYWHERE, (AFUNPTR)CountAllInstructions,
                IARG_UINT32, BBL_NumIns(bbl), IARG_END);
#endif
        for (INS ins = BBL_InsHead(bbl); INS_Valid(ins); ins = INS_Next(ins))
        {
            //First check routine
            RTN routine = INS_Rtn(ins);
            if (RTN_Valid(routine))
                routineName = RTN_Name(routine);
            else
                routineName = "unknown";

#ifdef ROUTINES_ON
            *out << routineName << " " << INS_Mnemonic(ins) << endl;
#endif

#ifndef ROUTINES_ON
            if ((routineName.find("mkl_vml") != string::npos) ||
                (routineName.find("Eigen") != string::npos))
            {
#ifdef STATS_ON
              if(isFMA(INS_Opcode(ins)))
                INS_InsertCall(ins, IPOINT_BEFORE, (AFUNPTR)CountVMLInstructions,
                              IARG_UINT32, 1, IARG_END);
#endif
                continue;
            }
            if ((mode == FMA_MP_FP32_WU_BN) || (mode == FMA_BF16_FP32_WU_BN))
            {
                if ((routineName.find("saxpy") != string::npos) ||
                        (routineName.find("axpy_axpby") != string::npos) ||
                        (routineName.find("saxpby") != string::npos) ||
                        (routineName.find("FusedBatchNorm") != string::npos) ||
                        (routineName.find("addcdiv") != string::npos) ||
                        (routineName.find("add_") != string::npos))
                {
#ifdef STATS_ON
                    INS_InsertCall(ins, IPOINT_BEFORE, (AFUNPTR)CountWUInstructions,
                            IARG_UINT32, 1, IARG_END);
#endif
                    continue;
                }
            }//closes mode == FMA_MP-FP32_WU_BN
            else
            {
                if ((mode == FMA_MP_FP32_WU) || (mode == FMA_BF16_FP32_WU))
                {
                    if ((routineName.find("saxpy") != string::npos) ||
                        (routineName.find("addcdiv") != string::npos) ||
                        (routineName.find("add_") != string::npos))
                    {
#ifdef STATS_ON
                        INS_InsertCall(ins, IPOINT_BEFORE, (AFUNPTR)CountWUInstructions,
                                IARG_UINT32, 1, IARG_END);
#endif
                        continue;
                    }
                } //closes mode == FMA_MP-FP32_WU
            }
            OPCODE iclass = INS_Opcode(ins);
#ifdef STATS_ON
            if(isFP(iclass))
            {
                INS_InsertCall(ins, IPOINT_BEFORE, (AFUNPTR)CountFPInstructions,
                        IARG_UINT32, 1, IARG_END);

            }
#endif
            // Check if it needs to be instrumented (is FMA)
            if (isFMA(iclass))
            {
#ifdef STATS_ON
                INS_InsertCall(ins, IPOINT_BEFORE, (AFUNPTR)CountFMAInstructions,
                        IARG_UINT32, 1, IARG_END);
#endif

#ifdef STATS_PROCESSED
                INS_InsertCall(ins, IPOINT_BEFORE, (AFUNPTR)CountFMAInstructions,
                        IARG_UINT32, 1, IARG_END);
#endif
                xed_decoded_inst_t* xed_ins = INS_XedDec(ins);

                numOperands = INS_OperandCount(ins);
                if (INS_IsMemoryRead(ins))
                {
                    REG operand1 = INS_OperandReg(ins, 0);
                    size = REG_Size(operand1);
                    assert(size == 64 || size == 32 || size == 16 || size == 8);
                    if (numOperands == 4)
                    {
                        REG mask = INS_OperandReg(ins, 1);
                        BOOL is_masking = xed_decoded_inst_masking(xed_ins);
                        BOOL is_zeroing = xed_decoded_inst_zeroing(xed_ins);
                        BOOL is_merging = xed_decoded_inst_merging(xed_ins);
                        REG operand2 = INS_OperandReg(ins, 2);
                        unsigned int bytes_read = INS_MemoryReadSize(ins);
                        switch(size)
                        {
                            case 64:
                            {
                                if (bytes_read == 4)
                                {
                                    if(isVFMA132(iclass))
                                    {
#ifdef LOGGING
                                        *out << INS_Disassemble(ins) << endl;
                                        *out << "Operand1 Size: " << size << endl;
                                        *out << "Operand2 Size: " << REG_Size(operand2) << endl;
                                        *out << "Mask Size: " << REG_Size(mask) << endl;
                                        *out << "IsMasking: " << xed_decoded_inst_masking(xed_ins) << endl;
                                        *out << "IsZeroing: " << xed_decoded_inst_zeroing(xed_ins) << endl;
                                        *out << "IsMerging: " << xed_decoded_inst_merging(xed_ins) << endl;
#endif
                                        INS_InsertCall(ins, IPOINT_BEFORE,
                                                 (AFUNPTR)InstrumentationOperandsBcastDecorator,
                                                 IARG_UINT32, INS_Opcode(ins),
                                                 IARG_REG_REFERENCE, operand1,
                                                 IARG_MEMORYREAD_EA,
                                                 IARG_REG_REFERENCE, operand2,
                                                 IARG_REG_REFERENCE, operand1,
                                                 IARG_CALL_ORDER, CALL_ORDER_FIRST,
                                                 IARG_END);
#ifndef LOGGING
#ifndef STATS_PROCESSED
                                        INS_Delete(ins);
#endif
#endif

#ifdef LOGGING
                                        INS_InsertCall(ins, IPOINT_AFTER,
                                                (AFUNPTR)PrintRealDestinationContent,
                                                IARG_REG_REFERENCE, operand1,
                                                IARG_END);
#endif

                                    } // closes if(isVFMA132(iclass))
                                    else
                                    {
                                        if(isVFMA213(iclass))
                                        {
#ifdef LOGGING
                                            *out << INS_Disassemble(ins) << endl;
                                            *out << "Operand1 Size: " << size << endl;
                                            *out << "Operand2 Size: " << REG_Size(operand2) << endl;
                                            *out << "Mask Size: " << REG_Size(mask) << endl;
                                            *out << "IsMasking: " << xed_decoded_inst_masking(xed_ins) << endl;
                                            *out << "IsZeroing: " << xed_decoded_inst_zeroing(xed_ins) << endl;
                                            *out << "IsMerging: " << xed_decoded_inst_merging(xed_ins) << endl;
#endif
                                            INS_InsertCall(ins, IPOINT_BEFORE,
                                                    (AFUNPTR)InstrumentationOperandsBcastDecorator2,
                                                    IARG_UINT32, INS_Opcode(ins),
                                                    IARG_REG_REFERENCE, operand2,
                                                    IARG_REG_REFERENCE, operand1,
                                                    IARG_MEMORYREAD_EA,
                                                    IARG_REG_REFERENCE, operand1,
                                                    IARG_CALL_ORDER, CALL_ORDER_FIRST,
                                                    IARG_END);
#ifndef LOGGING
#ifndef STATS_PROCESSED
                                            INS_Delete(ins);
#endif
#endif

#ifdef LOGGING
                                            INS_InsertCall(ins, IPOINT_AFTER,
                                                    (AFUNPTR)PrintRealDestinationContent,
                                                    IARG_REG_REFERENCE, operand1,
                                                    IARG_END);
#endif

                                        } // closes if(isVFMA213(iclass))
                                        else
                                        {
                                            if(isVFMA231(iclass))
                                            {
#ifdef LOGGING
                                                *out << INS_Disassemble(ins) << endl;
                                                *out << "Operand1 Size: " << size << endl;
                                                *out << "Operand2 Size: " << REG_Size(operand2) << endl;
                                                *out << "Mask Size: " << REG_Size(mask) << endl;
                                                *out << "IsMasking: " << xed_decoded_inst_masking(xed_ins) << endl;
                                                *out << "IsZeroing: " << xed_decoded_inst_zeroing(xed_ins) << endl;
                                                *out << "IsMerging: " << xed_decoded_inst_merging(xed_ins) << endl;
#endif
                                                INS_InsertCall(ins, IPOINT_BEFORE,
                                                        (AFUNPTR)InstrumentationOperandsBcastDecorator,
                                                        IARG_UINT32, INS_Opcode(ins),
                                                        IARG_REG_REFERENCE, operand2,
                                                        IARG_MEMORYREAD_EA,
                                                        IARG_REG_REFERENCE, operand1,
                                                        IARG_REG_REFERENCE, operand1,
                                                        IARG_CALL_ORDER, CALL_ORDER_FIRST,
                                                        IARG_END);
#ifndef LOGGING
#ifndef STATS_PROCESSED
                                                INS_Delete(ins);
#endif
#endif

#ifdef LOGGING
                                                INS_InsertCall(ins, IPOINT_AFTER,
                                                        (AFUNPTR)PrintRealDestinationContent,
                                                        IARG_REG_REFERENCE, operand1,
                                                        IARG_END);
#endif

                                            } // closes if(isVFMA231(iclass))
                                        }
                                    }
                                } // closes if(bytes_read == 4)
                                else
                                {
                                        if(isVFMA132(iclass))
                                        {
#ifdef LOGGING
                                            *out << INS_Disassemble(ins) << endl;
                                            *out << "Operand1 Size: " << size << endl;
                                            *out << "Operand2 Size: " << REG_Size(operand2) << endl;
                                            *out << "Mask Size: " << REG_Size(mask) << endl;
                                            *out << "IsMasking: " << xed_decoded_inst_masking(xed_ins) << endl;
                                            *out << "IsZeroing: " << xed_decoded_inst_zeroing(xed_ins) << endl;
                                            *out << "IsMerging: " << xed_decoded_inst_merging(xed_ins) << endl;
                                            xed_operand_action_enum_t xed_op1_action =
                                                xed_decoded_inst_operand_action(xed_ins, 0);
                                            xed_operand_action_enum_t xed_op2_action =
                                                xed_decoded_inst_operand_action(xed_ins, 2);
                                            *out << "What to merge Op1: " << xed_operand_action_enum_t2str(xed_op1_action) << endl;
                                            *out << "What to merge Op2: " << xed_operand_action_enum_t2str(xed_op2_action) << endl;
#endif
                                            INS_InsertCall(ins, IPOINT_BEFORE,
                                                    (AFUNPTR)InstrumentationOperandsMem64Bytes,
                                                    IARG_UINT32, INS_Opcode(ins),
                                                    IARG_REG_REFERENCE, operand1,
                                                    IARG_MEMORYREAD_EA,
                                                    IARG_REG_REFERENCE, operand2,
                                                    IARG_REG_REFERENCE, operand1,
                                                    IARG_REG_REFERENCE, mask,
                                                    IARG_BOOL, is_masking,
                                                    IARG_BOOL, is_zeroing,
                                                    IARG_BOOL, is_merging,
                                                    IARG_CALL_ORDER, CALL_ORDER_FIRST,
                                                    IARG_END);
#ifndef LOGGING
#ifndef STATS_PROCESSED
                                            INS_Delete(ins);
#endif
#endif

#ifdef LOGGING
                                            INS_InsertCall(ins, IPOINT_AFTER,
                                                    (AFUNPTR)PrintRealDestinationContent,
                                                    IARG_REG_REFERENCE, operand1,
                                                    IARG_END);
#endif

                                        } // closes if(isVFMA132(iclass))
                                        else
                                        {
                                            if(isVFMA213(iclass))
                                            {
#ifdef LOGGING
                                                *out << INS_Disassemble(ins) << endl;
                                                *out << "Operand1 Size: " << size << endl;
                                                *out << "Operand2 Size: " << REG_Size(operand2) << endl;
                                                *out << "Mask Size: " << REG_Size(mask) << endl;
                                                *out << "IsMasking: " << xed_decoded_inst_masking(xed_ins) << endl;
                                                *out << "IsZeroing: " << xed_decoded_inst_zeroing(xed_ins) << endl;
                                                *out << "IsMerging: " << xed_decoded_inst_merging(xed_ins) << endl;
                                                xed_operand_action_enum_t xed_op1_action =
                                                    xed_decoded_inst_operand_action(xed_ins, 0);
                                                xed_operand_action_enum_t xed_op2_action =
                                                    xed_decoded_inst_operand_action(xed_ins, 2);
                                                *out << "What to merge Op1: " << xed_operand_action_enum_t2str(xed_op1_action) << endl;
                                                *out << "What to merge Op2: " << xed_operand_action_enum_t2str(xed_op2_action) << endl;
#endif
                                                INS_InsertCall(ins, IPOINT_BEFORE,
                                                        (AFUNPTR)InstrumentationOperandsMem64Bytes2,
                                                        IARG_UINT32, INS_Opcode(ins),
                                                        IARG_REG_REFERENCE, operand2,
                                                        IARG_REG_REFERENCE, operand1,
                                                        IARG_MEMORYREAD_EA,
                                                        IARG_REG_REFERENCE, operand1,
                                                        IARG_REG_REFERENCE, mask,
                                                        IARG_CALL_ORDER, CALL_ORDER_FIRST,
                                                        IARG_END);
#ifndef LOGGING
#ifndef STATS_PROCESSED
                                                INS_Delete(ins);
#endif
#endif

#ifdef LOGGING
                                                INS_InsertCall(ins, IPOINT_AFTER,
                                                        (AFUNPTR)PrintRealDestinationContent,
                                                        IARG_REG_REFERENCE, operand1,
                                                        IARG_END);
#endif

                                            } // closes if(isVFMA213(iclass))
                                            else
                                            {
                                                if(isVFMA231(iclass))
                                                {
#ifdef LOGGING
                                                    *out << INS_Disassemble(ins) << endl;
                                                    *out << "Operand1 Size: " << size << endl;
                                                    *out << "Operand2 Size: " << REG_Size(operand2) << endl;
                                                    *out << "Mask Size: " << REG_Size(mask) << endl;
                                                    *out << "IsMasking: " << xed_decoded_inst_masking(xed_ins) << endl;
                                                    *out << "IsZeroing: " << xed_decoded_inst_zeroing(xed_ins) << endl;
                                                    *out << "IsMerging: " << xed_decoded_inst_merging(xed_ins) << endl;
                                                    xed_operand_action_enum_t xed_op1_action =
                                                        xed_decoded_inst_operand_action(xed_ins, 0);
                                                    xed_operand_action_enum_t xed_op2_action =
                                                        xed_decoded_inst_operand_action(xed_ins, 2);
                                                    *out << "What to merge Op1: " << xed_operand_action_enum_t2str(xed_op1_action) << endl;
                                                    *out << "What to merge Op2: " << xed_operand_action_enum_t2str(xed_op2_action) << endl;
#endif
                                                    INS_InsertCall(ins, IPOINT_BEFORE,
                                                            (AFUNPTR)InstrumentationOperandsMem64Bytes,
                                                            IARG_UINT32, INS_Opcode(ins),
                                                            IARG_REG_REFERENCE, operand2,
                                                            IARG_MEMORYREAD_EA,
                                                            IARG_REG_REFERENCE, operand1,
                                                            IARG_REG_REFERENCE, operand1,
                                                            IARG_REG_REFERENCE, mask,
                                                            IARG_BOOL, is_masking,
                                                            IARG_BOOL, is_zeroing,
                                                            IARG_BOOL, is_merging,
                                                            IARG_CALL_ORDER, CALL_ORDER_FIRST,
                                                            IARG_END);
#ifndef LOGGING
#ifndef STATS_PROCESSED
                                                    INS_Delete(ins);
#endif
#endif

#ifdef LOGGING
                                                    INS_InsertCall(ins, IPOINT_AFTER,
                                                            (AFUNPTR)PrintRealDestinationContent,
                                                            IARG_REG_REFERENCE, operand1,
                                                            IARG_END);
#endif

                                                } // closes if(isVFMA231(iclass))
                                            } // closes else if(isVFMA213(iclass))
                                        } // closes else if(isVFMA132(iclass))
                                    //} // close if (bytes_read == 64)
                                } // closes else if(bytes_read == 4)
                                break;
                            } // Closes case 64. To avoid affecting variables declared in the previous case
                            case 32:
                                break;
                            case 16:
                                break;
                            case 8:
                                break;
                            default:
                                break;
                        } // closes switch(size)
                    } // closes if (numOperands == 4)
                    else
                    {
                        if (numOperands == 3)
                        {
                            // The First operand is a register.
                            // First operand already in Operand1 variable
                            // Operand2 now could be read from position 1
                            // in the instruction. There is no masked
                            // operations
                            REG operand2 = INS_OperandReg(ins, 1);
                            // I will check for the number of bytes read
                            // by the current instruction. Avoiding conditionals
                            // in analysis routines in PIN reduces latency.
                            unsigned int bytes_read = INS_MemoryReadSize(ins);
                            switch(size)
                            {
                                case 64:
                                    {
                                        if (bytes_read == 4)
                                        {
                                            if(isVFMA132(iclass))
                                            {
#ifdef LOGGING
                                                *out << INS_Disassemble(ins) << endl;
                                                *out << "Operand1 Size: " << size << endl;
                                                *out << "Operand2 Size: " << REG_Size(operand2) << endl;
#endif
                                                INS_InsertCall(ins, IPOINT_BEFORE,
                                                        (AFUNPTR)InstrumentationOperandsBcastDecorator,
                                                        IARG_UINT32, INS_Opcode(ins),
                                                        IARG_REG_REFERENCE, operand1,
                                                        IARG_MEMORYREAD_EA,
                                                        IARG_REG_REFERENCE, operand2,
                                                        IARG_REG_REFERENCE, operand1,
                                                        IARG_CALL_ORDER, CALL_ORDER_FIRST,
                                                        IARG_END);
#ifndef LOGGING
#ifndef STATS_PROCESSED
                                                INS_Delete(ins);
#endif
#endif

#ifdef LOGGING
                                                INS_InsertCall(ins, IPOINT_AFTER,
                                                        (AFUNPTR)PrintRealDestinationContent,
                                                        IARG_REG_REFERENCE, operand1,
                                                        IARG_END);
#endif

                                            } // closes if(isVFMA132(iclass))
                                            else
                                            {
                                                if(isVFMA213(iclass))
                                                {
#ifdef LOGGING
                                                    *out << INS_Disassemble(ins) << endl;
                                                    *out << "Operand1 Size: " << size << endl;
                                                    *out << "Operand2 Size: " << REG_Size(operand2) << endl;
#endif
                                                    INS_InsertCall(ins, IPOINT_BEFORE,
                                                            (AFUNPTR)InstrumentationOperandsBcastDecorator2,
                                                            IARG_UINT32, INS_Opcode(ins),
                                                            IARG_REG_REFERENCE, operand2,
                                                            IARG_REG_REFERENCE, operand1,
                                                            IARG_MEMORYREAD_EA,
                                                            IARG_REG_REFERENCE, operand1,
                                                            IARG_CALL_ORDER, CALL_ORDER_FIRST,
                                                            IARG_END);
#ifndef LOGGING
#ifndef STATS_PROCESSED
                                                    INS_Delete(ins);
#endif
#endif

#ifdef LOGGING
                                                    INS_InsertCall(ins, IPOINT_AFTER,
                                                            (AFUNPTR)PrintRealDestinationContent,
                                                            IARG_REG_REFERENCE, operand1,
                                                            IARG_END);
#endif

                                                } // closes if(isVFMA213(iclass))
                                                else
                                                {
                                                    if(isVFMA231(iclass))
                                                    {
#ifdef LOGGING
                                                        *out << INS_Disassemble(ins) << endl;
                                                        *out << "Operand1 Size: " << size << endl;
                                                        *out << "Operand2 Size: " << REG_Size(operand2) << endl;
#endif
                                                        INS_InsertCall(ins, IPOINT_BEFORE,
                                                                (AFUNPTR)InstrumentationOperandsBcastDecorator,
                                                                IARG_UINT32, INS_Opcode(ins),
                                                                IARG_REG_REFERENCE, operand2,
                                                                IARG_MEMORYREAD_EA,
                                                                IARG_REG_REFERENCE, operand1,
                                                                IARG_REG_REFERENCE, operand1,
                                                                IARG_CALL_ORDER, CALL_ORDER_FIRST,
                                                                IARG_END);
#ifndef LOGGING
#ifndef STATS_PROCESSED
                                                        INS_Delete(ins);
#endif
#endif

#ifdef LOGGING
                                                        INS_InsertCall(ins, IPOINT_AFTER,
                                                                (AFUNPTR)PrintRealDestinationContent,
                                                                IARG_REG_REFERENCE, operand1,
                                                                IARG_END);
#endif
                                                    } // closes if(isVFMA231(iclass))
                                                }
                                            }
                                        } // closes if(bytes_read == 4)
                                        else
                                        {
                                        //    if (bytes_read == 64)
                                        //    {
                                                if(isVFMA132(iclass))
                                                {
#ifdef LOGGING
                                                    *out << INS_Disassemble(ins) << endl;
                                                    *out << "Operand1 Size: " << size << endl;
                                                    *out << "Operand2 Size: " << REG_Size(operand2) << endl;
#endif
                                                    INS_InsertCall(ins, IPOINT_BEFORE,
                                                            (AFUNPTR)InstrumentationOperandsMem64Bytes,
                                                            IARG_UINT32, INS_Opcode(ins),
                                                            IARG_REG_REFERENCE, operand1,
                                                            IARG_MEMORYREAD_EA,
                                                            IARG_REG_REFERENCE, operand2,
                                                            IARG_REG_REFERENCE, operand1,
                                                            IARG_REG_REFERENCE, operand1, // this value is not used in this case
                                                            IARG_BOOL, false,
                                                            IARG_BOOL, false,
                                                            IARG_BOOL, false,                                                            IARG_CALL_ORDER, CALL_ORDER_FIRST,
                                                            IARG_CALL_ORDER, CALL_ORDER_FIRST,
                                                            IARG_END);
#ifndef LOGGING
#ifndef STATS_PROCESSED
                                                    INS_Delete(ins);
#endif
#endif

#ifdef LOGGING
                                                    INS_InsertCall(ins, IPOINT_AFTER,
                                                            (AFUNPTR)PrintRealDestinationContent,
                                                            IARG_REG_REFERENCE, operand1,
                                                            IARG_END);
#endif

                                                } // closes if(isVFMA132(iclass))
                                                else
                                                {
                                                    if(isVFMA213(iclass))
                                                    {
#ifdef LOGGING
                                                        *out << INS_Disassemble(ins) << endl;
                                                        *out << "Operand1 Size: " << size << endl;
                                                        *out << "Operand2 Size: " << REG_Size(operand2) << endl;
#endif
                                                        INS_InsertCall(ins, IPOINT_BEFORE,
                                                                (AFUNPTR)InstrumentationOperandsMem64Bytes2,
                                                                IARG_UINT32, INS_Opcode(ins),
                                                                IARG_REG_REFERENCE, operand2,
                                                                IARG_REG_REFERENCE, operand1,
                                                                IARG_MEMORYREAD_EA,
                                                                IARG_REG_REFERENCE, operand1,
                                                                IARG_REG_REFERENCE, operand1, // is not used
                                                                IARG_BOOL, false,
                                                                IARG_BOOL, false,
                                                                IARG_BOOL, false,
                                                                IARG_CALL_ORDER, CALL_ORDER_FIRST,
                                                                IARG_END);
#ifndef LOGGING
#ifndef STATS_PROCESSED
                                                        INS_Delete(ins);
#endif
#endif

#ifdef LOGGING
                                                        INS_InsertCall(ins, IPOINT_AFTER,
                                                                (AFUNPTR)PrintRealDestinationContent,
                                                                IARG_REG_REFERENCE, operand1,
                                                                IARG_END);
#endif

                                                    } // closes if(isVFMA213(iclass))
                                                    else
                                                    {
                                                        if(isVFMA231(iclass))
                                                        {
#ifdef LOGGING
                                                            *out << INS_Disassemble(ins) << endl;
                                                            *out << "Operand1 Size: " << size << endl;
                                                            *out << "Operand2 Size: " << REG_Size(operand2) << endl;
#endif
                                                            INS_InsertCall(ins, IPOINT_BEFORE,
                                                                    (AFUNPTR)InstrumentationOperandsMem64Bytes,
                                                                    IARG_UINT32, INS_Opcode(ins),
                                                                    IARG_REG_REFERENCE, operand2,
                                                                    IARG_MEMORYREAD_EA,
                                                                    IARG_REG_REFERENCE, operand1,
                                                                    IARG_REG_REFERENCE, operand1,
                                                                    IARG_REG_REFERENCE, operand1, // this value is not used in this case
                                                                    IARG_BOOL, false,
                                                                    IARG_BOOL, false,
                                                                    IARG_BOOL, false,
                                                                    IARG_CALL_ORDER, CALL_ORDER_FIRST,
                                                                    IARG_END);
#ifndef LOGGING
#ifndef STATS_PROCESSED
                                                            INS_Delete(ins);
#endif
#endif

#ifdef LOGGING
                                                            INS_InsertCall(ins, IPOINT_AFTER,
                                                                    (AFUNPTR)PrintRealDestinationContent,
                                                                    IARG_REG_REFERENCE, operand1,
                                                                    IARG_END);
#endif
                                                        } // closes if(isVFMA231(iclass))
                                                    } // closes else if(isVFMA213(iclass))
                                                } // closes else if(isVFMA132(iclass))
                                           // } // close if (bytes_read == 64)
                                        } // closes else if(bytes_read == 4)
                                        break;
                                    } // Closes case 64. To avoid affecting variables declared in the previous case
                                case 32:
                                    {
                                        //if (bytes_read == 32)
                                        //{
                                            if(isVFMA132(iclass))
                                            {
#ifdef LOGGING
                                                *out << INS_Disassemble(ins) << endl;
                                                *out << "Operand1 Size: " << size << endl;
                                                *out << "Operand2 Size: " << REG_Size(operand2) << endl;
#endif
                                                INS_InsertCall(ins, IPOINT_BEFORE,
                                                        (AFUNPTR)InstrumentationOperandsMem32Bytes,
                                                        IARG_UINT32, INS_Opcode(ins),
                                                        IARG_REG_REFERENCE, operand1,
                                                        IARG_MEMORYREAD_EA,
                                                        IARG_REG_REFERENCE, operand2,
                                                        IARG_REG_REFERENCE, operand1,
                                                        IARG_CALL_ORDER, CALL_ORDER_FIRST,
                                                        IARG_END);
#ifndef LOGGING
#ifndef STATS_PROCESSED
                                                INS_Delete(ins);
#endif
#endif

#ifdef LOGGING
                                                INS_InsertCall(ins, IPOINT_AFTER,
                                                        (AFUNPTR)PrintRealDestinationContent256,
                                                        IARG_REG_REFERENCE, operand1,
                                                        IARG_END);
#endif

                                            } // closes if(isVFMA132(iclass))
                                            else
                                            {
                                                if(isVFMA213(iclass))
                                                {
#ifdef LOGGING
                                                    *out << INS_Disassemble(ins) << endl;
                                                    *out << "Operand1 Size: " << size << endl;
                                                    *out << "Operand2 Size: " << REG_Size(operand2) << endl;
#endif
                                                    INS_InsertCall(ins, IPOINT_BEFORE,
                                                            (AFUNPTR)InstrumentationOperandsMem32Bytes2,
                                                            IARG_UINT32, INS_Opcode(ins),
                                                            IARG_REG_REFERENCE, operand2,
                                                            IARG_REG_REFERENCE, operand1,
                                                            IARG_MEMORYREAD_EA,
                                                            IARG_REG_REFERENCE, operand1,
                                                            IARG_CALL_ORDER, CALL_ORDER_FIRST,
                                                            IARG_END);
#ifndef LOGGING
#ifndef STATS_PROCESSED
                                                    INS_Delete(ins);
#endif
#endif

#ifdef LOGGING
                                                    INS_InsertCall(ins, IPOINT_AFTER,
                                                            (AFUNPTR)PrintRealDestinationContent256,
                                                            IARG_REG_REFERENCE, operand1,
                                                            IARG_END);
#endif

                                                } // closes if(isVFMA213(iclass))
                                                else
                                                {
                                                    if(isVFMA231(iclass))
                                                    {
#ifdef LOGGING
                                                        *out << INS_Disassemble(ins) << endl;
                                                        *out << "Operand1 Size: " << size << endl;
                                                        *out << "Operand2 Size: " << REG_Size(operand2) << endl;
#endif
                                                        INS_InsertCall(ins, IPOINT_BEFORE,
                                                                (AFUNPTR)InstrumentationOperandsMem32Bytes,
                                                                IARG_UINT32, INS_Opcode(ins),
                                                                IARG_REG_REFERENCE, operand2,
                                                                IARG_MEMORYREAD_EA,
                                                                IARG_REG_REFERENCE, operand1,
                                                                IARG_REG_REFERENCE, operand1,
                                                                IARG_CALL_ORDER, CALL_ORDER_FIRST,
                                                                IARG_END);
#ifndef LOGGING
#ifndef STATS_PROCESSED
                                                        INS_Delete(ins);
#endif
#endif

#ifdef LOGGING
                                                        INS_InsertCall(ins, IPOINT_AFTER,
                                                                (AFUNPTR)PrintRealDestinationContent256,
                                                                IARG_REG_REFERENCE, operand1,
                                                                IARG_END);
#endif
                                                    } // closes if(isVFMA231(iclass))
                                                } // closes else if(isVFMA213(iclass))
                                            } // closes else if(isVFMA132(iclass))
                                        //} // close if (bytes_read == 32)
                                        break;
                                    }
                                case 16:
                                    break;
                                case 8:
                                    break;
                                default:
                                    break;
                            } // closes switch(size)
                        } // closes if (numOperands == 3)
                    } // close else if (numOperands == 4)
                } // closes if (INS_IsMemoryRead(ins))
                else
                {
                    // This operand is always the destination
                    // in all the FMA cases
                    REG operand1 = INS_OperandReg(ins, 0);
                    // I need to obtain the size of the operands.
                    // INS_OperandReg is always existent. No matter
                    // which FMA I am analyzing. That's why I'm
                    // using it. With this size I will check how to
                    // deal with the instruction (AVX512, AVX2, AVX)
                    size = REG_Size(operand1);
                    assert(size == 64 || size == 32 || size == 16 || size == 8);
                    if (numOperands == 4)
                    {
                        // When numOperands is equal to 4. The
                        // INS_OperandReg(ins, 1) returns the
                        // masks bits to be used if needed.
                        REG mask = INS_OperandReg(ins, 1);
                        // The Second operand is a register.
                        // Also the third one.
                        REG operand2 = INS_OperandReg(ins, 2);
                        REG operand3 = INS_OperandReg(ins, 3);
                        BOOL is_masking = xed_decoded_inst_masking(xed_ins);
                        BOOL is_zeroing = xed_decoded_inst_zeroing(xed_ins);
                        BOOL is_merging = xed_decoded_inst_merging(xed_ins);

                        switch(size)
                        {
                            case 64:
                            {
                                if(isVFMA132(iclass))
                                {
#ifdef LOGGING
                                    *out << INS_Disassemble(ins) << endl;
                                    *out << "Operand1 Size: " << size << endl;
                                    *out << "Operand2 Size: " << REG_Size(operand2) << endl;
                                    *out << "Mask Size: " << REG_Size(mask) << endl;
                                    *out << "IsMasking: " << xed_decoded_inst_masking(xed_ins) << endl;
                                    *out << "IsZeroing: " << xed_decoded_inst_zeroing(xed_ins) << endl;
                                    *out << "IsMerging: " << xed_decoded_inst_merging(xed_ins) << endl;
                                    xed_operand_action_enum_t xed_op1_action =
                                        xed_decoded_inst_operand_action(xed_ins, 0);
                                    xed_operand_action_enum_t xed_op2_action =
                                        xed_decoded_inst_operand_action(xed_ins, 2);
                                    xed_operand_action_enum_t xed_op3_action =
                                        xed_decoded_inst_operand_action(xed_ins, 3);
                                    *out << "What to merge Op1: " << xed_operand_action_enum_t2str(xed_op1_action) << endl;
                                    *out << "What to merge Op2: " << xed_operand_action_enum_t2str(xed_op2_action) << endl;
                                    *out << "What to merge Op3: " << xed_operand_action_enum_t2str(xed_op3_action) << endl;
#endif
                                    INS_InsertCall(ins, IPOINT_BEFORE,
                                            (AFUNPTR)InstrumentationOperandsRegs,
                                            IARG_UINT32, INS_Opcode(ins),
                                            IARG_REG_REFERENCE, operand1,
                                            IARG_REG_REFERENCE, operand3,
                                            IARG_REG_REFERENCE, operand2,
                                            IARG_REG_REFERENCE, operand1,
                                            IARG_REG_REFERENCE, mask,
                                            IARG_BOOL, is_masking,
                                            IARG_BOOL, is_zeroing,
                                            IARG_BOOL, is_merging,
                                            IARG_CALL_ORDER, CALL_ORDER_FIRST,
                                            IARG_END);
#ifndef LOGGING
#ifndef STATS_PROCESSED
                                    INS_Delete(ins);
#endif
#endif

#ifdef LOGGING
                                    INS_InsertCall(ins, IPOINT_AFTER,
                                            (AFUNPTR)PrintRealDestinationContent,
                                            IARG_REG_REFERENCE, operand1,
                                            IARG_END);
#endif

                                } // closes if(isVFMA132(iclass))
                                else
                                {
                                    if(isVFMA213(iclass))
                                    {
#ifdef LOGGING
                                        *out << INS_Disassemble(ins) << endl;
                                        *out << "Operand1 Size: " << size << endl;
                                        *out << "Operand2 Size: " << REG_Size(operand2) << endl;
                                        *out << "Mask Size: " << REG_Size(mask) << endl;
                                        *out << "IsMasking: " << xed_decoded_inst_masking(xed_ins) << endl;
                                        *out << "IsZeroing: " << xed_decoded_inst_zeroing(xed_ins) << endl;
                                        *out << "IsMerging: " << xed_decoded_inst_merging(xed_ins) << endl;
                                        xed_operand_action_enum_t xed_op1_action =
                                            xed_decoded_inst_operand_action(xed_ins, 0);
                                        xed_operand_action_enum_t xed_op2_action =
                                            xed_decoded_inst_operand_action(xed_ins, 2);
                                        xed_operand_action_enum_t xed_op3_action =
                                            xed_decoded_inst_operand_action(xed_ins, 3);
                                        *out << "What to merge Op1: " << xed_operand_action_enum_t2str(xed_op1_action) << endl;
                                        *out << "What to merge Op2: " << xed_operand_action_enum_t2str(xed_op2_action) << endl;
                                        *out << "What to merge Op3: " << xed_operand_action_enum_t2str(xed_op3_action) << endl;
#endif
                                        INS_InsertCall(ins, IPOINT_BEFORE,
                                                (AFUNPTR)InstrumentationOperandsRegs,
                                                IARG_UINT32, INS_Opcode(ins),
                                                IARG_REG_REFERENCE, operand2,
                                                IARG_REG_REFERENCE, operand1,
                                                IARG_REG_REFERENCE, operand3,
                                                IARG_REG_REFERENCE, operand1,
                                                IARG_REG_REFERENCE, mask,
                                                IARG_BOOL, is_masking,
                                                IARG_BOOL, is_zeroing,
                                                IARG_BOOL, is_merging,
                                                IARG_CALL_ORDER, CALL_ORDER_FIRST,
                                                IARG_END);
#ifndef LOGGING
#ifndef STATS_PROCESSED
                                        INS_Delete(ins);
#endif
#endif

#ifdef LOGGING
                                        INS_InsertCall(ins, IPOINT_AFTER,
                                                (AFUNPTR)PrintRealDestinationContent,
                                                IARG_REG_REFERENCE, operand1,
                                                IARG_END);
#endif

                                    } // closes if(isVFMA213(iclass))
                                    else
                                    {
                                        if(isVFMA231(iclass))
                                        {
#ifdef LOGGING
                                            *out << INS_Disassemble(ins) << endl;
                                            *out << "Operand1 Size: " << size << endl;
                                            *out << "Operand2 Size: " << REG_Size(operand2) << endl;
                                            *out << "Mask Size: " << REG_Size(mask) << endl;
                                            *out << "IsMasking: " << xed_decoded_inst_masking(xed_ins) << endl;
                                            *out << "IsZeroing: " << xed_decoded_inst_zeroing(xed_ins) << endl;
                                            *out << "IsMerging: " << xed_decoded_inst_merging(xed_ins) << endl;
                                            xed_operand_action_enum_t xed_op1_action =
                                                xed_decoded_inst_operand_action(xed_ins, 0);
                                            xed_operand_action_enum_t xed_op2_action =
                                                xed_decoded_inst_operand_action(xed_ins, 2);
                                            xed_operand_action_enum_t xed_op3_action =
                                                xed_decoded_inst_operand_action(xed_ins, 3);
                                            *out << "What to merge Op1: " << xed_operand_action_enum_t2str(xed_op1_action) << endl;
                                            *out << "What to merge Op2: " << xed_operand_action_enum_t2str(xed_op2_action) << endl;
                                            *out << "What to merge Op3: " << xed_operand_action_enum_t2str(xed_op3_action) << endl;
#endif
                                            INS_InsertCall(ins, IPOINT_BEFORE,
                                                    (AFUNPTR)InstrumentationOperandsRegs,
                                                    IARG_UINT32, INS_Opcode(ins),
                                                    IARG_REG_REFERENCE, operand2,
                                                    IARG_REG_REFERENCE, operand3,
                                                    IARG_REG_REFERENCE, operand1,
                                                    IARG_REG_REFERENCE, operand1,
                                                    IARG_REG_REFERENCE, mask,
                                                    IARG_BOOL, is_masking,
                                                    IARG_BOOL, is_zeroing,
                                                    IARG_BOOL, is_merging,
                                                    IARG_CALL_ORDER, CALL_ORDER_FIRST,
                                                    IARG_END);
#ifndef LOGGING
#ifndef STATS_PROCESSED
                                            INS_Delete(ins);
#endif
#endif

#ifdef LOGGING
                                            INS_InsertCall(ins, IPOINT_AFTER,
                                                    (AFUNPTR)PrintRealDestinationContent,
                                                    IARG_REG_REFERENCE, operand1,
                                                    IARG_END);
#endif
                                        } // closes if(isVFMA231(iclass))
                                    }
                                }
                                break;
                            }
                            case 32:
                                break;
                            case 16:
                                break;
                            case 8:
                                break;
                            default:
                                break;
                        }// closes switch(size) all elements from registers
                    } // closes if (numOperands == 4) where all operand are registers
                    else
                    {
                        // NumOperands = 3, all operands from registers
                        if (numOperands == 3)
                        {
                            REG operand2 = INS_OperandReg(ins, 1);
                            REG operand3 = INS_OperandReg(ins, 2);
                            switch(size)
                            {
                                case 64:
                                    {
                                        if(isVFMA132(iclass))
                                        {
#ifdef LOGGING
                                            *out << INS_Disassemble(ins) << endl;
                                            *out << "Operand1 Size: " << size << endl;
                                            *out << "Operand2 Size: " << REG_Size(operand2) << endl;
#endif
                                            INS_InsertCall(ins, IPOINT_BEFORE,
                                                    (AFUNPTR)InstrumentationOperandsRegs,
                                                    IARG_UINT32, INS_Opcode(ins),
                                                    IARG_REG_REFERENCE, operand1,
                                                    IARG_REG_REFERENCE, operand3,
                                                    IARG_REG_REFERENCE, operand2,
                                                    IARG_REG_REFERENCE, operand1,
                                                    IARG_REG_REFERENCE, operand1, // is not used
                                                    IARG_BOOL, false,
                                                    IARG_BOOL, false,
                                                    IARG_BOOL, false,
                                                    IARG_CALL_ORDER, CALL_ORDER_FIRST,
                                                    IARG_END);
#ifndef LOGGING
#ifndef STATS_PROCESSED
                                            INS_Delete(ins);
#endif
#endif

#ifdef LOGGING
                                            INS_InsertCall(ins, IPOINT_AFTER,
                                                    (AFUNPTR)PrintRealDestinationContent,
                                                    IARG_REG_REFERENCE, operand1,
                                                    IARG_END);
#endif

                                        } // closes if(isVFMA132(iclass))
                                        else
                                        {
                                            if(isVFMA213(iclass))
                                            {
#ifdef LOGGING
                                                *out << INS_Disassemble(ins) << endl;
                                                *out << "Operand1 Size: " << size << endl;
                                                *out << "Operand2 Size: " << REG_Size(operand2) << endl;
#endif
                                                INS_InsertCall(ins, IPOINT_BEFORE,
                                                        (AFUNPTR)InstrumentationOperandsRegs,
                                                        IARG_UINT32, INS_Opcode(ins),
                                                        IARG_REG_REFERENCE, operand2,
                                                        IARG_REG_REFERENCE, operand1,
                                                        IARG_REG_REFERENCE, operand3,
                                                        IARG_REG_REFERENCE, operand1,
                                                        IARG_REG_REFERENCE, operand1, // is not used
                                                        IARG_BOOL, false,
                                                        IARG_BOOL, false,
                                                        IARG_BOOL, false,
                                                        IARG_CALL_ORDER, CALL_ORDER_FIRST,
                                                        IARG_END);
#ifndef LOGGING
#ifndef STATS_PROCESSED
                                                INS_Delete(ins);
#endif
#endif

#ifdef LOGGING
                                                INS_InsertCall(ins, IPOINT_AFTER,
                                                        (AFUNPTR)PrintRealDestinationContent,
                                                        IARG_REG_REFERENCE, operand1,
                                                        IARG_END);
#endif

                                            } // closes if(isVFMA213(iclass))
                                            else
                                            {
                                                if(isVFMA231(iclass))
                                                {
#ifdef LOGGING
                                                    *out << INS_Disassemble(ins) << endl;
                                                    *out << "Operand1 Size: " << size << endl;
                                                    *out << "Operand2 Size: " << REG_Size(operand2) << endl;
#endif
                                                    INS_InsertCall(ins, IPOINT_BEFORE,
                                                            (AFUNPTR)InstrumentationOperandsRegs,
                                                            IARG_UINT32, INS_Opcode(ins),
                                                            IARG_REG_REFERENCE, operand2,
                                                            IARG_REG_REFERENCE, operand3,
                                                            IARG_REG_REFERENCE, operand1,
                                                            IARG_REG_REFERENCE, operand1,
                                                            IARG_REG_REFERENCE, operand1, // is not used
                                                            IARG_BOOL, false,
                                                            IARG_BOOL, false,
                                                            IARG_BOOL, false,
                                                            IARG_CALL_ORDER, CALL_ORDER_FIRST,
                                                            IARG_END);
#ifndef LOGGING
#ifndef STATS_PROCESSED
                                                    INS_Delete(ins);
#endif
#endif

#ifdef LOGGING
                                                    INS_InsertCall(ins, IPOINT_AFTER,
                                                            (AFUNPTR)PrintRealDestinationContent,
                                                            IARG_REG_REFERENCE, operand1,
                                                            IARG_END);
#endif
                                                } // closes if(isVFMA231(iclass))
                                            }
                                        }
                                        break;
                                    } // closes case 64
                                case 32:
                                    {
                                        if(isVFMA132(iclass))
                                        {
#ifdef LOGGING
                                            *out << INS_Disassemble(ins) << endl;
                                            *out << "Operand1 Size: " << size << endl;
                                            *out << "Operand2 Size: " << REG_Size(operand2) << endl;
#endif
                                            INS_InsertCall(ins, IPOINT_BEFORE,
                                                    (AFUNPTR)InstrumentationOperandsRegs256,
                                                    IARG_UINT32, INS_Opcode(ins),
                                                    IARG_REG_REFERENCE, operand1,
                                                    IARG_REG_REFERENCE, operand3,
                                                    IARG_REG_REFERENCE, operand2,
                                                    IARG_REG_REFERENCE, operand1,
                                                    IARG_CALL_ORDER, CALL_ORDER_FIRST,
                                                    IARG_END);
#ifndef LOGGING
#ifndef STATS_PROCESSED
                                            INS_Delete(ins);
#endif
#endif

#ifdef LOGGING
                                            INS_InsertCall(ins, IPOINT_AFTER,
                                                    (AFUNPTR)PrintRealDestinationContent256,
                                                    IARG_REG_REFERENCE, operand1,
                                                    IARG_END);
#endif

                                        } // closes if(isVFMA132(iclass))
                                        else
                                        {
                                            if(isVFMA213(iclass))
                                            {
#ifdef LOGGING
                                                *out << INS_Disassemble(ins) << endl;
                                                *out << "Operand1 Size: " << size << endl;
                                                *out << "Operand2 Size: " << REG_Size(operand2) << endl;
#endif
                                                INS_InsertCall(ins, IPOINT_BEFORE,
                                                        (AFUNPTR)InstrumentationOperandsRegs256,
                                                        IARG_UINT32, INS_Opcode(ins),
                                                        IARG_REG_REFERENCE, operand2,
                                                        IARG_REG_REFERENCE, operand1,
                                                        IARG_REG_REFERENCE, operand3,
                                                        IARG_REG_REFERENCE, operand1,
                                                        IARG_CALL_ORDER, CALL_ORDER_FIRST,
                                                        IARG_END);
#ifndef LOGGING
#ifndef STATS_PROCESSED
                                                INS_Delete(ins);
#endif
#endif
#ifdef LOGGING
                                                INS_InsertCall(ins, IPOINT_AFTER,
                                                        (AFUNPTR)PrintRealDestinationContent256,
                                                        IARG_REG_REFERENCE, operand1,
                                                        IARG_END);
#endif
                                            } // closes if(isVFMA213(iclass))
                                            else
                                            {
                                                if(isVFMA231(iclass))
                                                {
#ifdef LOGGING
                                                    *out << INS_Disassemble(ins) << endl;
                                                    *out << "Operand1 Size: " << size << endl;
                                                    *out << "Operand2 Size: " << REG_Size(operand2) << endl;
#endif
                                                    INS_InsertCall(ins, IPOINT_BEFORE,
                                                            (AFUNPTR)InstrumentationOperandsRegs256,
                                                            IARG_UINT32, INS_Opcode(ins),
                                                            IARG_REG_REFERENCE, operand2,
                                                            IARG_REG_REFERENCE, operand3,
                                                            IARG_REG_REFERENCE, operand1,
                                                            IARG_REG_REFERENCE, operand1,
                                                            IARG_CALL_ORDER, CALL_ORDER_FIRST,
                                                            IARG_END);
#ifndef LOGGING
#ifndef STATS_PROCESSED
                                                    INS_Delete(ins);
#endif
#endif
#ifdef LOGGING
                                                    INS_InsertCall(ins, IPOINT_AFTER,
                                                            (AFUNPTR)PrintRealDestinationContent256,
                                                            IARG_REG_REFERENCE, operand1,
                                                            IARG_END);
#endif
                                                } // closes if(isVFMA231(iclass))
                                            }
                                        }
                                        break;
                                    } // closes case 32
                                case 16:
                                    break;
                                case 8:
                                    break;
                                default:
                                    break;
                            }// closes switch(size) all elements from registers
                        } // closes if (numOperands == 3)
                    } // closes else if (numOperands == 4)
                } // closes else if (INS_IsMemoryRead(ins)) basically when all elements in the instruction come from registers
            } //closes if(isFMA)
#endif //closes #ifdef ROUTINES_ON
        } // closes for (INS ins = BBL_InsHead(bbl); INS_Valid(ins); ins = INS_Next(ins))
    } // closes for (BBL bbl = TRACE_BblHead(trace); BBL_Valid(bbl); bbl = BBL_Next(bbl))
} // closes Trace function


#ifdef STATS_PROCESSED
void print_stats_processed()
{
    *out << "Statistics Processed:" << endl;
    *out << "\tnum_fma_insts: " << num_fma_insts << endl;
    *out << "\tnum_processed_fma_insts: " << num_fma_processed_insts << endl;
}
#endif

void print_stats()
{
    *out << "Statistics:" << endl;
    *out << "\tnum_WU_insts: " << num_WU_insts << endl;
    *out << "\tnum_vml_insts: " << num_vml_insts << endl;
    *out << "\tnum_fp_insts: " << num_fp_insts << endl;
    *out << "\tnum_fma_insts: " << num_fma_insts << endl;
    *out << "\tnum_total_insts: " << num_total_insts << endl;
    *out << "\tnum_instrumented_regs: " << num_instrumented_regs << endl;
}

// This function is called when the application exits
// It prints the name and count for each procedure
VOID Fini(INT32 code, VOID *v)
{

#ifdef STATS_PROCESSED
    print_stats_processed();
#endif

#ifdef STATS_ON
    print_stats();
#endif
    *out << "End of the PinTool" << endl;
}

/* ===================================================================== */
/* Print Help Message                                                    */
/* ===================================================================== */

INT32 Usage()
{
    cerr << "This Pintool truncate FP32 to BF16 on some Floating Point Instructions" << endl;
    cerr << endl << KNOB_BASE::StringKnobSummary() << endl;
    return -1;
}

// Call back function called when the application it is started
VOID ApplicationStart(VOID *arg)
{
    *out << "Application start ..." << endl;
    sleep(1);
}


// Call back function called when the PinTool it is going to be
// detached from the application.
VOID DetachCompleted(VOID *arg)
{
    *out << "Detached completed ..." << endl;
    sleep(1);
}


// Callback function executed when the PinTool it is going to be
// attached to the application.
VOID attachMain(VOID* arg)
{
    TRACE_AddInstrumentFunction(Trace, 0);
    PIN_AddApplicationStartFunction(ApplicationStart, 0);
    PIN_AddDetachFunction(DetachCompleted, 0);
}

// Thread function to re-attach the PIN tool
static VOID attachThread(VOID *arg)
{
    int fd;
    char c;

    *out << "Thread Launched..." << endl;
    while (1)
    {
        *out << "Waiting in fifopipe" << endl;
        fd = open("fifopipe", O_RDONLY);
        read(fd, &c, 1);
        *out << "Received: " << c << endl;
        switch (c)
        {
        case 'A':
            mode = FMA_BF16;
            break;
        case 'B':
            mode = FMA_MP_FP32_WU_BN;
            break;
        case 'C':
            mode = FMA_MP_FP32_WU;
            break;
        case 'D':
            mode = FMA_MP;
            break;
        case 'E':
            mode = FMA_BF16_FP32_WU_BN;
            break;
        case 'F':
            mode = FMA_BF16_FP32_WU;
            break;
        default:
            mode = NATIVE;
            close(fd);
            PIN_Detach();
            *out << "Detached ..." << endl;
            continue;
        }
        close(fd);
        // First Detach
        PIN_Detach();
        sleep(4);
        *out << "Detached ..." << endl;
        while (ATTACH_FAILED_DETACH == PIN_Attach(attachMain, (VOID*)0))
        {
            *out << "Attach Failed " << mode << endl;
            exit(-1);
        }
        *out << "Attached " << mode << endl;
    }
}

KNOB<string> KnobOutputFile(KNOB_MODE_WRITEONCE, "pintool",
    "o", "fase.out", "specify output file name");
/* ===================================================================== */
/* Main                                                                  */
/* ===================================================================== */
int main(int argc, char * argv[])
{
    // Initialize symbol table code, needed for rtn instrumentation
    PIN_InitSymbols();
    scratchReg = PIN_ClaimToolRegister();
    if (!REG_valid(scratchReg))
    {
        std::cerr << "Cannot allocate a scratch register.\n";
        std::cerr << std::flush;
        return 1;
    }

    // Initialize pin
    if (PIN_Init(argc, argv)) return Usage();

    out = new ofstream(KnobOutputFile.Value().c_str());

    // Initial mode baseline FMA_MP_FP32_WU_BN
    mode = NATIVE;
    TRACE_AddInstrumentFunction(Trace_Native, 0);
    PIN_AddApplicationStartFunction(ApplicationStart, 0);
    PIN_AddDetachFunction(DetachCompleted, 0);

    // Register Fini to be called when the application exits
    PIN_AddFiniFunction(Fini, 0);

    // Create a thread inside the PIN Tool to do the re-attach
    // process
    THREADID tid = PIN_SpawnInternalThread(attachThread, NULL, 0x40000, NULL);
    assert(tid != INVALID_THREADID);

    // Start the program, never returns
    PIN_StartProgram();

    return 0;
}
