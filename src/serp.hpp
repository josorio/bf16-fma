#ifndef __dynamic_hpp__
#define __dynamic_hpp__

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <string.h>
#include <sched.h>
#include <immintrin.h>
#include <math.h>
#include <assert.h>
#include <set>
#include <bitset>
#include "pin.H"
extern "C" {
#include "xed-interface.h"
}

/**
 * Convert a FLT32 variable into the string representation of its value as an 8
 * digit hex number, padded with 0's.
 *
 * @param[in] fp Variable to convert.
 */
#define FLT32_TO_HEX(fp) \
    StringHex(*reinterpret_cast<const UINT32 *>(&fp), 8, FALSE)


using namespace LEVEL_CORE;


ofstream *out = 0;


// Register to use when RewriteMemOperand function
// is used
static REG scratchReg;

enum enumFMA
    {
        BN,
        WU,
        OTHER
    };


#ifdef AVX512



// Logging functions for __m512, __m256 and __mm128
template<class T>
inline void Log512(const __m512 &value)
{
    const size_t n = sizeof(__m512) / sizeof(T);
    T buffer[n];
    _mm512_storeu_ps((float*)buffer, value);

    for (unsigned int i = 0; i < n; i++)
        *out << "0x" << FLT32_TO_HEX(buffer[i]) << " ";

    *out << endl;
}
#endif

template<class T>
inline void Log256(const __m256 &value)
{
    const size_t n = sizeof(__m256) / sizeof(T);
    T buffer[n];
    _mm256_storeu_ps((float*)buffer, value);

    for (unsigned int i = 0; i < n; i++)
        *out << "0x" << FLT32_TO_HEX(buffer[i]) << " ";

    *out << endl;
}

template<class T>
inline void Log256i(const __m256i &value)
{
    const size_t n = sizeof(__m256i) / sizeof(T);
    T buffer[n];
    _mm256_store_epi32((T*)buffer, value);

    for (unsigned int i = 0; i < n; i++)
        *out << "0x" << FLT32_TO_HEX(buffer[i]) << " ";
    *out << endl;
}

template<class T>
inline void Log128(const __m128 &value)
{
    const size_t n = sizeof(__m128) / sizeof(T);
    T buffer[n];
    _mm_storeu_ps((float*)buffer, value);

    for (unsigned int i = 0; i < n; i++)
        *out << "0x" << FLT32_TO_HEX(buffer[i]) << " ";

    *out << endl;
}

// Operation modes
enum op_mode
{
    FMA_BF16,           // all bf16
    FMA_MP_FP32_WU_BN,  // baseline: bf16 + weight update (saxpy) in fp32 + FMA accumulator in 32b
    FMA_MP_FP32_WU,     // weight update (saxpy) in FP32
    FMA_MP,
    FMA_BF16_FP32_WU_BN,
    FMA_BF16_FP32_WU,
    NATIVE              // No instrumentation (detach)
};
op_mode mode;


// Function to check if a instruction is a
// floating point instruction and belongs
// to one of the instruction listed inside
// the conditional
bool isFP(OPCODE iclass)
{
    switch (iclass)
    {
    case XED_ICLASS_ADDSS:
        return true;
    case XED_ICLASS_SUBSS:
        return true;
    case XED_ICLASS_MULSS:
        return true;
    case XED_ICLASS_DIVSS:
        return true;
    case XED_ICLASS_ADDPS:
        return true;
    case XED_ICLASS_SUBPS:
        return true;
    case XED_ICLASS_MULPS:
        return true;
    case XED_ICLASS_DIVPS:
        return true;
    case XED_ICLASS_VADDSS:
        return true;
    case XED_ICLASS_VSUBSS:
        return true;
    case XED_ICLASS_VMULSS:
        return true;
    case XED_ICLASS_VDIVSS:
        return true;
    case XED_ICLASS_VADDPS:
        return true;
    case XED_ICLASS_VSUBPS:
        return true;
    case XED_ICLASS_VMULPS:
        return true;
    case XED_ICLASS_VDIVPS:
        return true;
    case XED_ICLASS_VFMADDSS:
        return true;
    case XED_ICLASS_VFMADDPS:
        return true;
    case XED_ICLASS_VFMADD132PS:
        return true;
    case XED_ICLASS_VFMADD213PS:
        return true;
    case XED_ICLASS_VFMADD231PS:
        return true;
    case XED_ICLASS_VFMADD132SS:
        return true;
    case XED_ICLASS_VFMADD213SS:
        return true;
    case XED_ICLASS_VFMADD231SS:
        return true;
    case XED_ICLASS_VFMADDSUB132PS:
        return true;
    case XED_ICLASS_VFMADDSUB213PS:
        return true;
    case XED_ICLASS_VFMADDSUB231PS:
        return true;
    case XED_ICLASS_VFMSUB132PS:
        return true;
    case XED_ICLASS_VFMSUB132SS:
        return true;
    case XED_ICLASS_VFMSUB213PS:
        return true;
    case XED_ICLASS_VFMSUB213SS:
        return true;
    case XED_ICLASS_VFMSUB231PS:
        return true;
    case XED_ICLASS_VFMSUB231SS:
        return true;
    case XED_ICLASS_VFMSUBADD132PS:
        return true;
    case XED_ICLASS_VFMSUBADD213PS:
        return true;
    case XED_ICLASS_VFMSUBADD231PS:
        return true;
    case XED_ICLASS_VFMSUBADDPS:
        return true;
    case XED_ICLASS_VFMSUBPS:
        return true;
    case XED_ICLASS_VFMSUBSS:
        return true;
    case XED_ICLASS_VFNMADD132PS:
        return true;
    case XED_ICLASS_VFNMADD132SS:
        return true;
    case XED_ICLASS_VFNMADD213PS:
        return true;
    case XED_ICLASS_VFNMADD213SS:
        return true;
    case XED_ICLASS_VFNMADD231PS:
        return true;
    case XED_ICLASS_VFNMADD231SS:
        return true;
    case XED_ICLASS_VFNMADDPS:
        return true;
    case XED_ICLASS_VFNMADDSS:
        return true;
    case XED_ICLASS_VFNMSUB132PS:
        return true;
    case XED_ICLASS_VFNMSUB132SS:
        return true;
    case XED_ICLASS_VFNMSUB213PS:
        return true;
    case XED_ICLASS_VFNMSUB213SS:
        return true;
    case XED_ICLASS_VFNMSUB231PS:
        return true;
    case XED_ICLASS_VFNMSUB231SS:
        return true;
    case XED_ICLASS_VFNMSUBPS:
        return true;
    case XED_ICLASS_VFNMSUBSS:
        return true;
    case XED_ICLASS_ADDSUBPS :
        return true;
    case XED_ICLASS_VADDSUBPS :
        return true;
    case XED_ICLASS_HADDPS :
        return true;
    case XED_ICLASS_VHADDPS :
        return true;
    case XED_ICLASS_HSUBPS :
        return true;
    case XED_ICLASS_VHSUBPS :
        return true;
    case XED_ICLASS_DPPS :
        return true;
    case XED_ICLASS_VDPPS :
        return true;
    case XED_ICLASS_RCPPS :
        return true;
    case XED_ICLASS_VRCPPS :
        return true;
    case XED_ICLASS_RSQRTPS :
        return true;
    case XED_ICLASS_VRSQRTPS :
        return true;
    case XED_ICLASS_RSQRTSS :
        return true;
    case XED_ICLASS_SQRTSS :
        return true;
    case XED_ICLASS_SQRTPS :
        return true;
    case XED_ICLASS_VSQRTPS :
        return true;
#ifdef AVX512
    case XED_ICLASS_V4FMADDPS:
        return true;
    case XED_ICLASS_V4FMADDSS:
        return true;
    case XED_ICLASS_V4FNMADDPS:
        return true;
    case XED_ICLASS_V4FNMADDSS:
        return true;
#endif
    }
    return false;
}

// Function to check if a instruction is a
// floating point instruction and belongs
// to one of the instruction listed inside
// the conditional
bool isFMA(OPCODE iclass)
{
    switch (iclass)
    {
    case XED_ICLASS_VFMADDPS:
    case XED_ICLASS_VFMADD132PS:
    case XED_ICLASS_VFMADD213PS:
    case XED_ICLASS_VFMADD231PS:
        return true;
    }
    return false;
}

bool isVFMA132(OPCODE iclass)
{
    switch (iclass)
    {
    case XED_ICLASS_VFMADD132PS:
    case XED_ICLASS_VFMADD132SS:
    case XED_ICLASS_VFMADDSUB132PS:
    case XED_ICLASS_VFMSUB132PS:
    case XED_ICLASS_VFMSUB132SS:
    case XED_ICLASS_VFMSUBADD132PS:
    case XED_ICLASS_VFNMADD132PS:
    case XED_ICLASS_VFNMADD132SS:
    case XED_ICLASS_VFNMSUB132PS:
    case XED_ICLASS_VFNMSUB132SS:
        return true;
    }
    return false;
}

bool isVFMA213(OPCODE iclass)
{
    switch (iclass)
    {
    case XED_ICLASS_VFMADDSS:
    case XED_ICLASS_VFMADDPS:
    case XED_ICLASS_VFMADD213PS:
    case XED_ICLASS_VFMADD213SS:
    case XED_ICLASS_VFMADDSUB213PS:
    case XED_ICLASS_VFMSUB213PS:
    case XED_ICLASS_VFMSUB213SS:
    case XED_ICLASS_VFMSUBADD213PS:
    case XED_ICLASS_VFMSUBADDPS:
    case XED_ICLASS_VFMSUBPS:
    case XED_ICLASS_VFMSUBSS:
    case XED_ICLASS_VFNMADD213PS:
    case XED_ICLASS_VFNMADD213SS:
    case XED_ICLASS_VFNMADDPS:
    case XED_ICLASS_VFNMADDSS:
    case XED_ICLASS_VFNMSUB213PS:
    case XED_ICLASS_VFNMSUB213SS:
    case XED_ICLASS_VFNMSUBPS:
    case XED_ICLASS_VFNMSUBSS:
        return true;
    }
    return false;
}

bool isVFMA231(OPCODE iclass)
{
    switch (iclass)
    {
    case XED_ICLASS_VFMADD231PS:
    case XED_ICLASS_VFMADD231SS:
    case XED_ICLASS_VFMADDSUB231PS:
    case XED_ICLASS_VFMSUB231PS:
    case XED_ICLASS_VFMSUB231SS:
    case XED_ICLASS_VFMSUBADD231PS:
    case XED_ICLASS_VFNMADD231PS:
    case XED_ICLASS_VFNMADD231SS:
    case XED_ICLASS_VFNMSUB231PS:
    case XED_ICLASS_VFNMSUB231SS:
        return true;
    }
    return false;
}
// Vectorized version as in tensorflow
// https://github.com/tensorflow/tensorflow/blob/0ff7955a0c1a42e2767afb0a5cc202dfe4d6ff19/tensorflow/core/lib/bfloat16/bfloat16.h#L184
// Return a __m512 to avoid vzeroupper instruction
#ifdef AVX512
inline __m512 ToBFloatTensorFlowVect64 (__m512* input) {

#ifdef LOGGING
    Log512<float>(*input);
#endif

    __m512i MSB_mask = _mm512_set1_epi32(0x80000000);
    __m512i LSB_mask = _mm512_set1_epi32(1);
    __m512i mask = _mm512_set1_epi32(0xFFFF0000);
    __m512i qnan_mask = _mm512_set1_epi32(0x7FC00000);
    __m512i rounding_mask = _mm512_set1_epi32(0x7FFF);

    // shift + get LSB bits + generate rounding bias
    __m512i tmp = _mm512_srli_epi32(*(__m512i*)input, 16);
    tmp = _mm512_and_si512(tmp, LSB_mask);
    __m512i rounding_bias = _mm512_add_epi32(tmp, rounding_mask);

    // Save MSB bits set
    __m512i MSB_set = _mm512_and_si512(*(__m512i*)input, MSB_mask);

    // Set all MSB to 0
    tmp = _mm512_xor_si512(*(__m512i*)input, MSB_set);

    // AVX does not have unsigned 32b addition :(
    // Do the addition now that all MSB are cleared
    tmp = _mm512_add_epi32(tmp, rounding_bias);

    // Reset MSB bits that were set to 1 originally
    tmp = _mm512_or_si512(tmp, MSB_set);
    // is nan? Use ordered comparison, if both NaN returns false, true otherwise
    __mmask16 not_isnan_mask = _mm512_cmp_ps_mask(*input, *input, _CMP_EQ_OQ);

    // Truncate and insert qNaN if input was NaN
    tmp = _mm512_mask_and_epi32(qnan_mask, not_isnan_mask, tmp, mask);

    // Return value to avoid vzeroupper
    *input = *(__m512*)&tmp;

#ifdef LOGGING
    Log512<float>(*input);
    *out << "-----------------" << endl;
#endif

    return *input;
}
#endif


// Vectorized version as in tensorflow
// https://github.com/tensorflow/tensorflow/blob/0ff7955a0c1a42e2767afb0a5cc202dfe4d6ff19/tensorflow/core/lib/bfloat16/bfloat16.h#L184
// Return a __m256 to avoid vzeroupper instruction
inline __m256 ToBFloatTensorFlowVect32 (__m256* input) {

#ifdef LOGGING
    Log256<float>(*input);
#endif

    __m256i MSB_mask = _mm256_set1_epi32(0x80000000);
    __m256i LSB_mask = _mm256_set1_epi32(1);
    __m256i mask = _mm256_set1_epi32(0xFFFF0000);
    __m256i qnan_mask = _mm256_set1_epi32(0x7FC00000);
    __m256i rounding_mask = _mm256_set1_epi32(0x7FFF);

    // shift + get LSB bits + generate rounding bias
    __m256i tmp = _mm256_srli_epi32(*(__m256i*)input, 16);
    tmp = _mm256_and_si256(tmp, LSB_mask);
    __m256i rounding_bias = _mm256_add_epi32(tmp, rounding_mask);

    // Save MSB bits set
    __m256i MSB_set = _mm256_and_si256(*(__m256i*)input, MSB_mask);

    // Set all MSB to 0
    tmp = _mm256_xor_si256(*(__m256i*)input, MSB_set);

    // AVX2 does not have unsigned 32b addition :(
    // Do the addition now that all MSB are cleared
    tmp = _mm256_add_epi32(tmp, rounding_bias);

    // Reset MSB bits that were set to 1 originally
    tmp = _mm256_or_si256(tmp, MSB_set);

    // Truncate
    tmp = _mm256_and_si256(tmp, mask);

    // is nan? Use ordered comparison, if both NaN returns false, true otherwise
    __m256 not_isnan_mask = _mm256_cmp_ps(*input, *input, _CMP_EQ_OQ);

    // negate not_isnan_mask
    __m256i isnan_mask = ~(*(__m256i*)&not_isnan_mask);

    // clear nans from current result, and clear non_nans from qnan_mask
    tmp = _mm256_and_si256(tmp, *(__m256i*)&not_isnan_mask);
    qnan_mask = _mm256_and_si256(qnan_mask, isnan_mask);

    // Merge to apply changes
    tmp = _mm256_or_si256(tmp, qnan_mask);

    // Return value to avoid vzeroupper
    *input = *(__m256*)&tmp;

#ifdef LOGGING
    Log256<float>(*input);
    *out << "-----------------" << endl;
#endif

    return *input;
}


inline __m256 ToBFloatSimpleRNEVect32(__m256* input)
{
#ifdef LOGGING
    Log256<float>(*input);
#endif

    __m256i hulp = _mm256_set1_epi32(1 << 15);
    __m256i mask = _mm256_set1_epi32(0xFFFF0000);
    __m256i MSB_mask = _mm256_set1_epi32(0x80000000);

    // Save MSB bits set
    __m256i MSB_set = _mm256_and_si256(*(__m256i*)input, MSB_mask);

    // Set all MSB to 0
    __m256i tmp = _mm256_xor_si256(*(__m256i*)input, MSB_set);

    // AVX2 does not have unsigned 32b addition :(
    // Do the addition now that all MSB are cleared
    tmp = _mm256_add_epi32(tmp, hulp);

    // Reset MSB bits that were set to 1 originally
    tmp = _mm256_or_si256(tmp, MSB_set);

    // Truncate
    tmp = _mm256_and_si256(tmp, mask);

#ifdef LOGGING
    Log256<float>(*(__m256*)&tmp);
    *out << "-----------------" << endl;
#endif

    *input = *(__m256*)&tmp;
    return *input;
}

inline __m128 ToBFloatSimpleRNEVect16(__m128* input)
{
#ifdef LOGGING
    Log128<float>(*input);
#endif

    __m128i hulp = _mm_set1_epi32(1 << 15);
    __m128i mask = _mm_set1_epi32(0xFFFF0000);
    __m128i MSB_mask = _mm_set1_epi32(0x80000000);

    // Save MSB bits set
    __m128i MSB_set = _mm_and_si128(*(__m128i*)input, MSB_mask);

    // Set all MSB to 0
    __m128i tmp = _mm_xor_si128(*(__m128i*)input, MSB_set);

    // AVX does not have unsigned 32b addition :(
    // Do the addition now that all MSB are cleared
    tmp = _mm_add_epi32(tmp, hulp);

    // Reset MSB bits that were set to 1 originally
    tmp = _mm_or_si128(tmp, MSB_set);

    // Truncate
    tmp = _mm_and_si128(tmp, mask);

#ifdef LOGGING
    Log128<float>(*(__m128*)&tmp);
    *out << "-----------------" << endl;
#endif

    *input = *(__m128*)&tmp;
    return *input;
}

// Truncate without rounding
inline __m256 ToBFloatTruncVect32(__m256* input)
{
#ifdef LOGGING
    Log256<float>(*input);
#endif

    __m256i mask = _mm256_set1_epi32(0xFFFF0000);

    __m256i tmp = *(__m256i*)input;
    tmp = _mm256_and_si256(tmp, mask);

#ifdef LOGGING
    Log256<float>(*(__m256*)&tmp);
    *out << "-----------------" << endl;
#endif

    *input = *(__m256*)&tmp;
    return *input;
}

// Truncate without rounding
inline __m128 ToBFloatTruncVect16(__m128* input)
{
#ifdef LOGGING
    Log128<float>(*input);
#endif

    __m128i mask = _mm_set1_epi32(0xFFFF0000);

    __m128i tmp = *(__m128i*)input;
    tmp = _mm_and_si128(tmp, mask);

#ifdef LOGGING
    Log128<float>(*(__m128*)&tmp);
    *out << "-----------------" << endl;
#endif

    *input = *(__m128*)&tmp;
    return *input;
}


// This union is used to make the conversion process
// to BF16 using the rounding method introduced by
// TensorFlow.
union FP32
{
    unsigned int u;
    float f;
};

inline FLT32 ToBFloatSimpleRNE(float floatNumber)
{
    uint32_t input;
    FP32 f;
    f.f = floatNumber;
    input = f.u;
    float output;
    uint32_t hulp = 1 << 15;
    uint32_t mask = 0xFFFF0000;

    uint32_t tmp = (input + hulp) & mask;
    output = *(float*)&tmp;
    return output;
}

inline FLT32 ToBFloatTrunc(float floatNumber)
{
    int temp;
    temp = *((int*)&floatNumber);
    temp = (temp & 0xFFFF0000);
    floatNumber = *(float*)&temp;
    return floatNumber;
}

union bfp16 {
    float            f;
    unsigned short   i[2];
};

/* we treat bfp16 as unsigned short here */
inline void convert_bfp16_fp32(const unsigned short* in, float* out, unsigned int len) {
    unsigned int i = 0;
    /* up-convert is super simple */
    for ( i = 0; i < len; ++i ) {
        union bfp16 t;
        t.i[1] = in[i];
        t.i[0] = 0;
        out[i] = t.f;
    }
}

/* we treat bfp16 as unsigned short here */
inline void rne_convert_fp32_bfp16(const float* in, unsigned short* out, const unsigned int len) {
    unsigned int i = 0;
    /* truncate buffer to bfp16 */
    for ( i = 0; i < len; ++i ) {
        unsigned int int_round = 0;
        unsigned int do_round = 1;
        memcpy( &int_round, &(in[i]), 4 );
        /* we don't round NaN and inf */
        if ( (int_round & 0x7f800000) == 0x7f800000 ) {
            do_round = 0;
        }
        /* perform round nearest tie even */
        if ( do_round != 0 ) {
            unsigned int fixup = (int_round >> 16) & 1;
            int_round = int_round + 0x00007fff + fixup;
        }

        /* create the bfp16 value by shifting out the lower 16bits */
        int_round = int_round >> 16;
        out[i] = (unsigned short)int_round;
    }
}

/**
 * This function takes a full precision FP32 and returns one BF16 number to representing it
 * */
inline void convert_float_to_one_bfloat_vec(__m512 *number, __m512 *x1)
{
    __m512 tmp2 = *number;
    *x1 = ToBFloatTensorFlowVect64((__m512*)&tmp2);
}

inline void convert_float_to_one_bfloat (float X, unsigned short *x1)
{
    float s;
    double dtmp = (double) X;

    /* Iteration 1: */
    s = (float) dtmp;
    rne_convert_fp32_bfp16 ( &s, x1, 1 );
}

/**
 * This function takes a full precision FP32 and returns two BF16 numbers representing it
 * */
inline void convert_float_to_two_bfloats_vec(__m512 *number, __m512 *x1, __m512 *x2)
{
    __m512 tmp = *number;
    __m512 tmp2 = *number;
    *x1 = ToBFloatTensorFlowVect64((__m512*)&tmp2);
    tmp = _mm512_sub_ps(tmp, *x1);

    *x2 = ToBFloatTensorFlowVect64((__m512*)&tmp);

}

/**
 * This function takes a full precision FP32 and returns three BF16 numbers representing it
 * */
inline void convert_float_to_three_bfloats_vec(__m512 *number, __m512 *x1, __m512 *x2, __m512 *x3)
{
    __m512 tmp = *number;
    __m512 tmp2 = *number;
    __m512 tmp3;
    *x1 = ToBFloatTensorFlowVect64((__m512*)&tmp2);
    tmp = _mm512_sub_ps(tmp, *x1);

    tmp3 = tmp;
    *x2 = ToBFloatTensorFlowVect64((__m512*)&tmp);
    tmp3 = _mm512_sub_ps(tmp3, *x2);

    *x3 = ToBFloatTensorFlowVect64((__m512*)&tmp3);

}


/**
 * This function takes a full precision FP32 __m256 vector and returns one BF16 number representing it
 * */
inline void convert_float_to_one_bfloat_vec_256(__m256 *number, __m256 *x1)
{
    __m256 tmp2 = *number;
    *x1 = ToBFloatTensorFlowVect32((__m256*)&tmp2);
}



/**
 * This function takes a full precision FP32 __m256 vector and returns two BF16 numbers representing it
 * */
inline void convert_float_to_two_bfloats_vec_256(__m256 *number, __m256 *x1, __m256 *x2)
{
    __m256 tmp = *number;
    __m256 tmp2 = *number;
    *x1 = ToBFloatTensorFlowVect32((__m256*)&tmp2);
    tmp = _mm256_sub_ps(tmp, *x1);

    *x2 = ToBFloatTensorFlowVect32((__m256*)&tmp);

}
/**
 * This function takes a full precision FP32 __m256 vector and returns three BF16 numbers representing it
 * */
inline void convert_float_to_three_bfloats_vec_256(__m256 *number, __m256 *x1, __m256 *x2, __m256 *x3)
{
    __m256 tmp = *number;
    __m256 tmp2 = *number;
    __m256 tmp3;
    *x1 = ToBFloatTensorFlowVect32((__m256*)&tmp2);
    tmp = _mm256_sub_ps(tmp, *x1);

    tmp3 = tmp;
    *x2 = ToBFloatTensorFlowVect32((__m256*)&tmp);
    tmp3 = _mm256_sub_ps(tmp3, *x2);

    *x3 = ToBFloatTensorFlowVect32((__m256*)&tmp3);

}

inline void convert_float_to_two_bfloats (float X, unsigned short *x1, unsigned short *x2)
{
    float s, stmp1, stmp2;
    double dtmp = (double) X;

    /* Iteration 1: */
    s = (float) dtmp;
    rne_convert_fp32_bfp16 ( &s, x1, 1 );
    convert_bfp16_fp32( x1, &stmp1, 1 );
    dtmp -= (double)stmp1;

    /* Iteration 2: */
    s = (float) dtmp;
    rne_convert_fp32_bfp16 ( &s, x2, 1 );
    convert_bfp16_fp32( x2, &stmp2, 1 );
    dtmp -= (double)stmp2;
}

inline void convert_float_to_three_bfloats ( float X, unsigned short *x1, unsigned short *x2,
                                      unsigned short *x3 )
{
    float s, stmp1, stmp2, stmp3;
    double dtmp = (double) X;

    /* Iteration 1: */
    s = (float) dtmp;
    rne_convert_fp32_bfp16 ( &s, x1, 1 );
    convert_bfp16_fp32( x1, &stmp1, 1 );
    dtmp -= (double)stmp1;

    /* Iteration 2: */
    s = (float) dtmp;
    rne_convert_fp32_bfp16 ( &s, x2, 1 );
    convert_bfp16_fp32( x2, &stmp2, 1 );
    dtmp -= (double)stmp2;

    /* Iteration 3: */
    s = (float) dtmp;
    rne_convert_fp32_bfp16 ( &s, x3, 1 );
    convert_bfp16_fp32( x3, &stmp3, 1 );
    dtmp -= (double)stmp3;

}

inline void LogBF16(unsigned short *value, int n)
{
    float stmp;
    for (int i = 0; i < n; i++)
    {
        convert_bfp16_fp32(&value[i], &stmp, 1);
        *out << "0x" << FLT32_TO_HEX(stmp) << " ";
    }
    *out << endl;
}

inline void LogFloat(float *value, int n)
{
    for (int i = 0; i < n; i++)
    {
        *out << "0x" << FLT32_TO_HEX(value[i]) << " ";
    }
    *out << endl;
}

inline __m512 to_bfloat_3_m512(__m512 *operand, unsigned int src_id)
{
#ifdef LOGGING
    *out << "Operand with id=" << src_id << endl;
    Log512<float>(*operand);
#endif
    const size_t n = sizeof(__m512) / sizeof(float);
    float buffer[n];

    unsigned short x1[16], x2[16], x3[16];

    _mm512_storeu_ps((float*)buffer, *operand); // each element will have a BF16 number
    for (int i = 0; i < int(n); i++)
    {
        convert_float_to_three_bfloats(buffer[i], &x1[i], &x2[i], &x3[i]);
    }
#ifdef LOGGING
    LogBF16(x1, int(n));
    LogBF16(x2, int(n));
    LogBF16(x3, int(n));
#endif
    return *operand;
}

/**
 * This function converts the FMA operands to BF16 triplets.
 * The destination is also updated.
 **/
inline void process_operands(__m512* operand1, __m512* operand2, __m512* operand3, __m512* destination,
                             __mmask16* mask, bool is_masking, bool is_zeroing, bool is_merging)
{
    __m512 sumind2, sumind3, sum1, sum2, sumind4;
    __m512 multiplier, sumfull;

    __m512 sx1_op1, sx2_op1;
    __m512 sx1_op2, sx2_op2;

    // To round the accumulator
    __m512 sx1_op3, sx2_op3;

    __m512 sx1_mult, sx2_mult;

    convert_float_to_two_bfloats_vec(operand1, (__m512*)&sx1_op1, (__m512*)&sx2_op1);
    convert_float_to_two_bfloats_vec(operand2, (__m512*)&sx1_op2, (__m512*)&sx2_op2);

    //Round the accumulator to BF16x2
    convert_float_to_two_bfloats_vec(operand3, (__m512*)&sx1_op3, (__m512*)&sx2_op3);

    //////////////// BF16x2 implementation with 3 products ///////////////////
    sumind4 = _mm512_mul_ps(sx2_op1, sx2_op2);

    sumind3 = _mm512_mul_ps(sx1_op1, sx2_op2);
    sumind3 = _mm512_fmadd_ps(sx2_op1, sx1_op2, sumind3);

    sumind2 = _mm512_mul_ps(sx1_op1, sx1_op2);

    multiplier = _mm512_add_ps(sumind4, sumind3);
    multiplier = _mm512_add_ps(multiplier, sumind2);
    convert_float_to_two_bfloats_vec((__m512*)&multiplier, (__m512*)&sx1_mult, (__m512*)&sx2_mult);

    sum1 = _mm512_add_ps(sx1_mult, sx1_op3);
    sum2 = _mm512_add_ps(sx2_mult, sx2_op3);
    sumfull = _mm512_add_ps(sum1, sum2);


    if (is_masking)
    {
        __m512 one_vec = _mm512_set1_ps(0xFFFFFFFF);
        if (is_zeroing)
        {
            sumfull = _mm512_maskz_and_ps(*mask, sumfull, one_vec);
        }
        else
        {
            sumfull = _mm512_mask_and_ps(*destination, *mask, sumfull, one_vec);
        }
    }

#ifdef TEST_MODIFICATION
    sumfull = _mm512_set1_ps(12343.56);
#endif

#ifdef LOGGING
    *out << "Operand1" << endl;
    Log512<float>(*operand1);
    *out << "Duplets" << endl;
    Log512<float>(sx1_op1);
    Log512<float>(sx2_op1);
    *out << "Operand2" << endl;
    Log512<float>(*operand2);
    *out << "Duplets" << endl;
    Log512<float>(sx1_op2);
    Log512<float>(sx2_op2);
    *out << "Operand3" << endl;
    Log512<float>(*operand3);
#endif
    // To implement the BF16x2 and BF16x3 conversion and
    // operations we need to skip the FMA instruction in PIN.
    // Additionally we need to write in the Destination Register
    // the result of the FMA. On FMA operations this register is
    // always the operand1
#ifndef LOGGING
    *destination = sumfull;
#endif

#ifdef LOGGING
   __m512 toCompare = _mm512_fmadd_ps(*operand1, *operand2, *operand3);
    *out << "Result Duplets:" << endl;
    Log512<float>(*destination);
    *out << "Result ToCompare:" << endl;
    Log512<float>(toCompare);
#endif

}


/**
 * This function converts the operands in a FMA to BF16 triplets. Use when the second operand is broadcasted
 * The destination is also updated.
 **/
inline void process_operands_bcast_operand2(__m512* operand1, float operand2, __m512* operand3, __m512* destination)
{
    unsigned short x1_op2, x2_op2; // The operand2 is not a vec.

    __m512 sumind2, sumind3, sum1, sum2, sumind4;
    __m512 sumfull, multiplier;
    __m512 sx1_op3, sx2_op3;

    __m512 sx1_op1, sx2_op1;
    __m512 sx1_mult, sx2_mult;
    float sx1_op2, sx2_op2; // Operand2 is not a vec.

    convert_float_to_two_bfloats_vec(operand1, (__m512*)&sx1_op1, (__m512*)&sx2_op1);
    convert_float_to_two_bfloats(operand2, &x1_op2, &x2_op2);
    // Operand 2 Triplet conversion to FP32
    convert_bfp16_fp32( &x1_op2, &sx1_op2, 1 );
    convert_bfp16_fp32( &x2_op2, &sx2_op2, 1 );

    //Round the accumulator to BF16x2
    convert_float_to_two_bfloats_vec(operand3, (__m512*)&sx1_op3, (__m512*)&sx2_op3);

    __m512 sx1_op2_vec = _mm512_set1_ps(sx1_op2);
    __m512 sx2_op2_vec = _mm512_set1_ps(sx2_op2);

    ///////////////////// BF16x2 implementation with 3 operations////////////////////
    sumind4 = _mm512_mul_ps(sx2_op1, sx2_op2_vec);

    sumind3 = _mm512_mul_ps(sx1_op1, sx2_op2_vec);
    sumind3 = _mm512_fmadd_ps(sx2_op1, sx1_op2_vec, sumind3);

    sumind2 = _mm512_mul_ps(sx1_op1, sx1_op2_vec);

    multiplier = _mm512_add_ps(sumind4, sumind3);
    multiplier = _mm512_add_ps(multiplier, sumind2);
    convert_float_to_two_bfloats_vec((__m512*)&multiplier, (__m512*)&sx1_mult, (__m512*)&sx2_mult);

    // We accumulate using the BF16x2 representation in the multiplier
    // Output and the accumulator
    sum1 = _mm512_add_ps(sx1_mult, sx1_op3);
    sum2 = _mm512_add_ps(sx2_mult, sx2_op3);
    sumfull = _mm512_add_ps(sum1, sum2);

#ifdef TEST_MODIFICATION
    sumfull = _mm512_set1_ps(12343.56);
#endif

#ifdef LOGGING
    *out << "Operand1" << endl;
    Log512<float>(*operand1);
    *out << "Duplets" << endl;
    Log512<float>(sx1_op1);
    Log512<float>(sx2_op1);
    *out << "Operand2" << endl;
    *out << operand2 << endl;
    *out << "Duplets" << endl;
    Log512<float>(sx1_op2_vec);
    Log512<float>(sx2_op2_vec);
    *out << "Operand3" << endl;
    Log512<float>(*operand3);
#endif

    // To implement the BF16x2 and BF16x3 conversion and
    // operations we need to skip the FMA instruction in PIN.
    // Additionally we need to write in the Destination Register
    // the result of the FMA. On FMA operations this register is
    // always the operand1
#ifndef LOGGING
    *destination = sumfull;
#endif

#ifdef LOGGING
    __m512 tmp_operand2 = _mm512_set1_ps(operand2);
    __m512 toCompare = _mm512_fmadd_ps(*operand1, tmp_operand2, *operand3);
    *out << "Result Triplets:" << endl;
    Log512<float>(*destination);
    *out << "Result ToCompare:" << endl;
    Log512<float>(toCompare);
#endif

}

/**
 * This function converts the operands in a FMA to BF16 triplets. Use when the third operand is broadcasted
 * The destination is also updated.
 **/
inline void process_operands_bcast_operand3(__m512* operand1, __m512* operand2, float operand3, __m512* destination)
{

    __m512 sumind2, sumind3, sum1, sum2, sumind4;
    __m512 sumfull, multiplier;

    __m512 sx1_op1, sx2_op1;
    __m512 sx1_op2, sx2_op2;
    __m512 sx1_mult, sx2_mult;

    float sx1_op3, sx2_op3;

    unsigned short x1_op3, x2_op3; //Operand3 is not a vec

    convert_float_to_two_bfloats_vec(operand1, (__m512*)&sx1_op1, (__m512*)&sx2_op1);
    convert_float_to_two_bfloats_vec(operand2, (__m512*)&sx1_op2, (__m512*)&sx2_op2);


    // The accumulator needs to be represented in BF16x2
    convert_float_to_two_bfloats(operand3, &x1_op3, &x2_op3);
    convert_bfp16_fp32(&x1_op3, &sx1_op3, 1);
    convert_bfp16_fp32(&x2_op3, &sx2_op3, 1);

    __m512 sx1_op3_vec = _mm512_set1_ps(sx1_op3);
    __m512 sx2_op3_vec = _mm512_set1_ps(sx2_op3);

    ////////////// BF16x2 implementation with 4 operations/////////////
    sumind4 = _mm512_mul_ps(sx2_op1, sx2_op2);

    sumind3 = _mm512_mul_ps(sx1_op1, sx2_op2);
    sumind3 = _mm512_fmadd_ps(sx2_op1, sx1_op2, sumind3);

    sumind2 = _mm512_mul_ps(sx1_op1, sx1_op2);

    multiplier = _mm512_add_ps(sumind4, sumind3);
    multiplier = _mm512_add_ps(multiplier, sumind2);
    convert_float_to_two_bfloats_vec((__m512*)&multiplier, (__m512*)&sx1_mult, (__m512*)&sx2_mult);

    sum1 = _mm512_add_ps(sx1_mult, sx1_op3_vec);
    sum2 = _mm512_add_ps(sx2_mult, sx2_op3_vec);
    sumfull = _mm512_add_ps(sum1, sum2);

#ifdef TEST_MODIFICATION
        sumfull[i] = 12343.56;
#endif

#ifdef LOGGING
    *out << "Operand1" << endl;
    Log512<float>(*operand1);
    *out << "Duplets" << endl;
    Log512<float>(sx1_op1);
    Log512<float>(sx2_op1);
    *out << "Operand2" << endl;
    Log512<float>(*operand2);
    *out << "Duplets" << endl;
    Log512<float>(sx1_op2);
    Log512<float>(sx2_op2);
    *out << "Operand3" << endl;
    *out << operand3 << endl;
#endif

    // To implement the BF16x2 and BF16x3 conversion and
    // operations we need to skip the FMA instruction in PIN.
    // Additionally we need to write in the Destination Register
    // the result of the FMA. On FMA operations this register is
    // always the operand1
#ifndef LOGGING
    *destination = sumfull;
#endif

#ifdef LOGGING
    __m512 tmp_operand3 = _mm512_set1_ps(operand3);
    __m512 toCompare = _mm512_fmadd_ps(*operand1, *operand2, tmp_operand3);
    *out << "Result Triplets:" << endl;
    Log512<float>(*destination);
    *out << "Result ToCompare:" << endl;
    Log512<float>(toCompare);
#endif

}

inline void process_operands_256(__m256* operand1, __m256* operand2, __m256* operand3, __m256* destination)
{
    __m256 sumind2, sumind3, sum1, sum2, sumind4;
    __m256 sumfull, multiplier;

    __m256 sx1_op1, sx2_op1;
    __m256 sx1_op2, sx2_op2;
    __m256 sx1_op3, sx2_op3;
    __m256 sx1_mult, sx2_mult;

    convert_float_to_two_bfloats_vec_256(operand1, (__m256*)&sx1_op1, (__m256*)&sx2_op1);
    convert_float_to_two_bfloats_vec_256(operand2, (__m256*)&sx1_op2, (__m256*)&sx2_op2);

    convert_float_to_two_bfloats_vec_256(operand3, (__m256*)&sx1_op3, (__m256*)&sx2_op3);


    ///////////////////// BF16x2 implementation with 3 operations//////////////////////////
    sumind4 = _mm256_mul_ps(sx2_op1, sx2_op2);

    sumind3 = _mm256_mul_ps(sx1_op1, sx2_op2);
    sumind3 = _mm256_fmadd_ps(sx2_op1, sx1_op2, sumind3);

    sumind2 = _mm256_mul_ps(sx1_op1, sx1_op2);

    multiplier = _mm256_add_ps(sumind4, sumind3);
    multiplier = _mm256_add_ps(multiplier, sumind2);
    convert_float_to_two_bfloats_vec_256((__m256*)&multiplier, (__m256*)&sx1_mult, (__m256*)&sx2_mult);

    sum1 = _mm256_add_ps(sx1_mult, sx1_op3);
    sum2 = _mm256_add_ps(sx2_mult, sx2_op3);
    sumfull = _mm256_add_ps(sum1, sum2);

#ifdef TEST_MODIFICATION
    sumfull = _mm256_set1_ps(12343.56);
#endif

#ifdef LOGGING
    *out << "Operand1" << endl;
    Log256<float>(*operand1);
    *out << "Triplets" << endl;
    Log256<float>(sx1_op1);
    Log256<float>(sx2_op1);
    *out << "Operand2" << endl;
    Log256<float>(*operand2);
    *out << "Triplets" << endl;
    Log256<float>(sx1_op2);
    Log256<float>(sx2_op2);
    *out << "Operand3" << endl;
    Log256<float>(*operand3);
#endif

    // To implement the BF16x2 and BF16x3 conversion and
    // operations we need to skip the FMA instruction in PIN.
    // Additionally we need to write in the Destination Register
    // the result of the FMA. On FMA operations this register is
    // always the operand1
    *destination = sumfull;
#ifdef LOGGING
    __m256 toCompare = _mm256_fmadd_ps(*operand1, *operand2, *operand3);
    *out << "Result Triplets" << endl;
    Log256<float>(toCompare);
    *out << "Result ToCompare" << endl;
    Log256<float>(*destination);
#endif
}

// Vectorized version as in tensorflow
inline __m128 ToBFloatTensorFlowVect16 (__m128* input) {

#ifdef LOGGING
    Log128<float>(*input);
#endif

    __m128i MSB_mask = _mm_set1_epi32(0x80000000);
    __m128i LSB_mask = _mm_set1_epi32(1);
    __m128i mask = _mm_set1_epi32(0xFFFF0000);
    __m128i qnan_mask = _mm_set1_epi32(0x7FC00000);
    __m128i rounding_mask = _mm_set1_epi32(0x7FFF);

    // shift + get LSB bits + generate rounding bias
    __m128i tmp = _mm_srli_epi32(*(__m128i*)input, 16);
    tmp = _mm_and_si128(tmp, LSB_mask);
    __m128i rounding_bias = _mm_add_epi32(tmp, rounding_mask);

    // Save MSB bits set
    __m128i MSB_set = _mm_and_si128(*(__m128i*)input, MSB_mask);

    // Set all MSB to 0
    tmp = _mm_xor_si128(*(__m128i*)input, MSB_set);

    //AVX2 does not have unsigned 32b addition :(
    //Do the addition now that all MSB are cleared
    tmp = _mm_add_epi32(tmp, rounding_bias);

    // Reset MSB bits that were set to 1 originally
    tmp = _mm_or_si128(tmp, MSB_set);

    // Truncate
    tmp = _mm_and_si128(tmp, mask);

    // is nan? Use ordered comparison, if both NaN returns false, true otherwise
    __m128 not_isnan_mask = _mm_cmp_ps(*input, *input, _CMP_EQ_OQ);

    // negate not_isnan_mask
    __m128i isnan_mask = ~(*(__m128i*)&not_isnan_mask);

    // clear nans from tmp, and clear non_nans from qnan_mask
    tmp = _mm_and_si128(tmp, *(__m128i*)&not_isnan_mask);
    qnan_mask = _mm_and_si128(qnan_mask, isnan_mask);

    // Merge to apply changes
    tmp = _mm_or_si128(tmp, qnan_mask);

    // Return value to avoid vzeroupper
    *input = *(__m128*)&tmp;

#ifdef LOGGING
    Log128<float>(*input);
    *out << "-----------------" << endl;
#endif

    return *input;
}

// This is the rounding method used by TensorFlow
FLT32 ToBFloatTensorFlow(float floatNumber){

    uint32_t input;
    FP32 f;
    f.f = floatNumber;
    input = f.u;

#ifdef LOGGING
    *out << "0x" << FLT32_TO_HEX(floatNumber) << " " << endl;
#endif

    uint32_t lsb = (input >> 16) & 1;
    uint32_t rounding_bias = 0x7fff + lsb;
    input += rounding_bias;

    int32_t temp = static_cast<int16_t>(input>>16);
    temp = temp << 16;

    // If the value is a NaN, squash it to a qNaN with msb of fraction set,
    // this makes sure after truncation we don't end up with an inf.
    if(isnan(floatNumber))
        temp = 0x7FC00000;
    floatNumber = *(float*)&temp;
#ifdef LOGGING
    *out << "0x" << FLT32_TO_HEX(floatNumber) << " " << endl;
    *out << "-----------------" << endl;
#endif
    return floatNumber;

}
#endif
