#!/bin/bash

export PIN_ROOT=../pin/pin-3.7-97619-g0d0c92f4f-gcc-linux
export PATH=$PIN_ROOT:$PATH

# As Pytorch and MKL are based on openMP define the number of threads
# equal to the number of physical processors you have access to
export OMP_NUM_THREADS=48

# Define a folder path to save the training results each epoch
CHECKPOINT_FILES=./results/train_resnet18_bf16x2

# To run the application with the pintool hooked to it we need to launch
# the following command.
pin -inline 1 -t ../src/obj-intel64/serp.so -- python main_cifar_100_bf16xN.py \
    --chk ${CHECKPOINT_FILES} --model ResNet18 --milestones 82 122 --training_data ./data
