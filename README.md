## Getting started

### Abstract

Fused Multiply-Add (FMA) functional units constitute a fundamental hardware component to train Deep Neural Networks (DNNs). Its silicon area grows quadratically with the mantissa bit count of the computer number format, which has motivated the adoption of the BrainFloat16 format (BF16). BF16 features 1 sign, 8 exponent and 7 explicit mantissa bits. Some approaches to train DNNs achieve significant performance benefits by using the BF16 format. However, these approaches must combine BF16 with the standard IEEE 754 Floating-Point 32-bit (FP32) format to achieve state-of-the-art training accuracy, which limits the impact of adopting BF16. This article proposes the first approach able to train complex DNNs entirely using the BF16 format. We propose a new class of FMA operators, FMAbf16n_m , that entirely rely on BF16 FMA hardware instructions and deliver the same accuracy as FP32. FMAbf16n_m operators achieve performance improvements within the 1.28-1.35× range on ResNet101 with respect to FP32. FMAbf16n_m enables training complex DNNs on simple low-end hardware devices without requiring expensive FP32 FMA functional units.

### Prerequisites
* GCC compiler (Tested with gcc 8.10)
* AVX512 support
* Compiled PyTorch version (MKLDNN/OneDNN support enabled)

### Installation
To test our FMAbf16n_m approach we need to use an emulation tool. To do so we are using SERP (Seamless Emulation of Reduced Precision Formats). First step is to install Intel PIN, the tool used by SERP. Just extract the content in the same pin folder and export a variable in the environment like the following.

```bash
export PIN_ROOT=../pin/pin-3.7-97619-g0d0c92f4f-gcc-linux
export PATH=$PIN_ROOT:$PATH
```

Then go to the src folder and run make there:

```bash
cd src
make
```

A .so file will be created inside a new folder called obj-64. That will the the binary to be used by PIN with the BF16 emulation on it. In this test we are supporting thg FMAbf162_2{4}.

Finally, to run the example run the .sh file inside resnet18 folder. In this case a training process of ResNet18 will be launched using CIFAR100 as the dataset. You will get checkpoints each epoch inside the results folder.
